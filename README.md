
# **Resumen**
- Proyecto que emula una página de compraventa de productos ficticios de la serie Mobile Suit Gundam del periodo Universal Century.
- La funcionalidad de la página se centra en la funcionalidad del catálogo sobre los clientes y las tiendas que se dan de alta.
  La página ofrece 4 tipos de usuarios:
  	- No dados de alta
  	- Clientes
  	- Tiendas
  	- Administradores


# **Before start**
- Antes de empezar a probar el proyecto en su entorno de trabajo tendrá que modificar los siguientes apartados:
    - Cambiar todas las url que aparecen en el proyecto por las de su ubicación (/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_AngularJS_1_4_9/pagina/).
    - Modificar o crear un fichero connect.ini con los datos de su base de datos y cargar el fichero .sql

# **Tecnologías Usadas**
	1. AngularJS 1.4.9
        1. DirPaginate
		2. AngularJS Material
			1. Dialog 
			2. Toasters
	2. PHP 5
	3. I18n
	4. JWT.io
	5. MAILGUN

# **Features** 
- [FrontEnd Documentation](documentation/frontend/index.md)
- [Backend  Documentation](documentation/backend/index.md)

# **Data Base Schema**
![SQL](documentation/media/sql.png)

# **TESTS**
Este proyecto cuenta con una serie de tests creados para probar las funcionalidades principales de la pagina. Cada test mencionado aqui se encuentra en la carpeta TESTS del repositorio. (Todos los tests son de mi autoria y o se han usado fuentes de terceros para su creacion).

## Frontend Tests
----

***Basics***

En este test se plasma el funcionamiento basico de AngularJS 1.4.9. Se explica:
* Como una aplicacion.
* Como crear un controlador con, y sin, la sintaxis Controller as.
* Crear Factorias de servicios.

***Custom Directives***

En este test se plasma un funcionamiento mas avanzado de AngularJS 1.4.9. Se explica:
* Como crear directivas parametrizadas.

***Compare Products***

Este tests plasma el funcionamiento del modulo de compare products. El test esta en una fase de funcionamiento mas prematura que en su implementacion final.

***Dropzone***

Este test muestra la factoria de servicios usada por la aplicacion para subir imagenes al servidor. El test descompone las imagenes en base 64 para que sea mas sencilla su manipulacion. 

***Internacionalizacion***

Este test muestra como implementar i18N en AngularJs 1.4.9 con ficheros json externos.

***Material Dialog y Toaster***

Estos dos tests se una reinterpretacion propia, a factoria de servicios, del funcionamiento que proporciona AngularJs Material.

## Backend Tests
----
***ORM***

En este test se explica como implementar el orm, de propia autoria, usado en la aplicacion.

***Routed Page***

En este test se explica el servicio que enruta el backend de la aplicacion.

***Logs***

En este test se explica el sitema de Logs usado en esta aplicacion.

***MailGun***

En este test se explica como utilizar los datos aportados por MailGun para recrear un servicio de Postmark.

# **Links**

[Material Toasters Base](https://material.angularjs.org/latest/demo/toast)

[Material Dialog Base](https://material.angularjs.org/latest/demo/toast)

[MailGun](https://www.mailgun.com/)



