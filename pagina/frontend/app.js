var gundam_proyect_root = angular.module('gundam_proyect_root', ['ngMaterial', 'ngMessages','pascalprecht.translate',"ngRoute",'angularUtils.directives.dirPagination']);
gundam_proyect_root.run(['$rootScope', function($rootScope) {
    if(localStorage.getItem("lang")){
        console.log( localStorage.getItem("lang"));
        $rootScope.lang=localStorage.getItem("lang");
    }else{
        console.log("no idiom");
        localStorage.setItem("lang","en");
        $rootScope.lang="en";
    }
       

}])
gundam_proyect_root.config(['$routeProvider','$translateProvider',function ($routeProvider,$translateProvider) {
        $translateProvider
            .useStaticFilesLoader({
                prefix: 'frontend/utils/lang/menu_',
                suffix: '.json'
            }) 
            // remove the warning from console log by putting the sanitize strategy
            .useSanitizeValueStrategy('sanitizeParameters')    
            .preferredLanguage(localStorage.getItem("lang"));   
        $routeProvider
            .when("/", {
                controller: "home_main_controller",
                controllerAs: "home_main_controller",
                templateUrl: "../pagina/frontend/modules/home/views/home.view.html",
                resolve: {
                    full_catalog: function (catalog_services) {
                        return  catalog_services.catalog("all");
                    }
                }
            })
            .when("/contact", {
               controller: "contact_controller",
                controllerAs: "contact_controller", 
                templateUrl: "../pagina/frontend/modules/contact/views/contact.view.html"
            })
         //catalog and stock
            .when("/catalog:param", {
               controller: "catalog_main_controller",
               controllerAs: "catalog_main_controller",
               templateUrl: "../pagina/frontend/modules/catalog/views/catalog.view.html" 
            })
            .when("/details:param", {
               controller: "catalog_main_controller",
               controllerAs: "catalog_main_controller",
               templateUrl: "../pagina/frontend/modules/catalog/views/catalog.view.html" 
            })
            .when("/shop_st:param", {
               controller: "catalog_main_controller",
               controllerAs: "catalog_main_controller",
               templateUrl: "../pagina/frontend/modules/catalog/views/catalog.view.html" 
            })
            .when("/shop_dt:param", {
               controller: "catalog_main_controller",
               controllerAs: "catalog_main_controller",
               templateUrl: "../pagina/frontend/modules/catalog/views/catalog.view.html" 
            })
         //catalog and stock
         //authentication & profile 
            .when("/login", {
                controller: "auth_controller",
                controllerAs: "auth_controller",
                templateUrl: "../pagina/frontend/modules/authentication/views/authentication.view.html"
            })
            .when("/verificate:token", {
                templateUrl: "../pagina/frontend/modules/authentication/views/authentication.view.html",
                resolve: {
                    token: function (services,$route,$window) {
                        var data={action:"validate_register",data:$route.current.params.token.substr(1)};
                        services.post("authentication","auth",data);
                        return $window.location.href="#/login";
                    }
                }
            })
            .when("/update/:to_update/:thing/:token", {
                templateUrl: "../pagina/frontend/modules/authentication/views/authentication.view.html",
                resolve: {
                    update: function (services,$route,$window) {
                        /* console.log($route.current.params); */
                        var data={action:"update",
                                 data:{
                                    to_update:$route.current.params.to_update.substr(1),
                                    thing:$route.current.params.thing.substr(1),
                                    token:$route.current.params.token.substr(1)}};
                            services.post("authentication","auth",data)
                        return $window.location.href="#/login";
                    }
                }
            })
            .when("/logout",{
                templateUrl: "../pagina/frontend/modules/home/views/home.view.html",
                resolve: {
                    logout: function (data_storage,$window) {
                        data_storage.destroy_users_sesion();
                        $window.location.href="#/";
                        $window.location.reload();
                    }
                }
            })
            .when("/profile",{
                controller: "auth_controller",
                controllerAs: "auth_controller",
                templateUrl: "../pagina/frontend/modules/authentication/views/profile_tabs.view.html",
            })
         //authentication & profile  
    }])