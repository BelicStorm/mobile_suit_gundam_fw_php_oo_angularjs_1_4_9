gundam_proyect_root.directive("addToStock",["toaster","catalog_services","data_storage",
    "services",function(toaster,catalog_services,data_storage,services){
    var controller = function () {
        var vm = this;
        function init() {
            vm.items = [{}];
        }
        init();
        
        vm.addItem = function () {
            //Add new customer to directive scope
            data_storage.get_user("token").then(function(token){
                console.log(vm.data);
                var data={action:"add_to_stock",data:{
                                                modelo_id:vm.data.modelo_id,
                                                name:vm.data.ms_name,
                                                current_price:vm.data.price_per_unit,
                                                token:token}};
               services.post("stock_shop","stock",data).then(function(resolve){
                   console.log(resolve);
                    toaster.toast(resolve);
               })
            })  
        };
        
      };
      var template = '<i ng-click="vm.addItem()" class="fa fa-shopping-cart"></i>';    

      return {
          restrict: 'EA', //Default for 1.3+
          scope: {
              data:"="
          },
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
          template: template
      };

}])