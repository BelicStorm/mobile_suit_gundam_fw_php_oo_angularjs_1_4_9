gundam_proyect_root.directive("catalog",["catalog_services","stock_services","data_storage","$compile",function(data_storage,stock_services,catalog_services,$compile){
    var controller = function (catalog_services,stock_services,data_storage) {
        var vm = this;
        function init() { 
            /* console.log(vm.filter); */
            if(vm.filter.split(" ")[1]=="shop_st"){
                data_storage.get_user("token").then(function(name){ //print the stock of the loged shop
                    var data={data:{token:name,option:"all"},action:"select_stock"};
                    stock_services.stock(data).then(function(resolve){
                        /* console.log(resolve); */
                        vm.items=resolve;
                    })
                })
            }else if(vm.filter.split(" ")[1]=="shop_dt"){
                vm.info= vm.filter.split(" ")[0].split("@");
                var data={data:{shop_name:vm.info[1],option:"one",item:vm.info[0]},action:"select_stock"};
                stock_services.stock(data).then(function(resolve){
                    console.log(resolve);
                    vm.items=resolve;
                })
                
            }else{
                catalog_services.catalog(vm.filter.split(" ")[0]).then(function(resolve){
                    vm.items=resolve;
                    /* console.log(vm.items); */
                })
            } 
        }
        init();
    };
    var all_stock_template='<form class="form-inline">'
                            +    '<div class="form-group">'       
                            +        '<label >Search</label>'
                            +        '<input type="text" ng-model="search" class="form-control" placeholder="Search">'
                            +    '</div>'
                            +'</form>'
                            +'<div ng-controller="compare_controller as compare">'//añadimos el controllador de comparaciones (compare_controller)
                            +   '<md-checkbox ng-model="compare.starter" aria-label="Checkbox 1">Compare Mode</md-checkbox>'
                            +   '<test ng-show="compare.selected2.product2 && compare.starter" type="button" id="compare.compare_container"></test>'
                            +   '<div class="flex-container">'
                            +       '<div dir-paginate="products in vm.items|filter:search|itemsPerPage:4">'
                            +           '<div class="flex-item ">'
                            +               '<span class="shop_tittle">{{products.modelo.user_name}} </span>'
                            +               '<img class="product_image" ng-src="{{products.url}}">'
                            +               '<a class="product_tittle">{{products.modelo.ms_name}}</a>'
                            +               '<a ng-hide="compare.starter" '
                            +                   'href="#/shop_dt:{{products.modelo.ms_name}}@{{products.modelo.user_name}}"><i class="fa fa-eye"></i></a>'
                            +               '<a ng-show="compare.starter" ng-click="compare.select(products)"'
                            +                   'ng-class="{'+"selected"+': products == compare.selected1.product1 || products == compare.selected2.product2}">'
                            +                   '<i class="fa fa-plus" ></i></a>'    
                            +           '</div>'
                            +       '</div>'
                            +   '</div>'
                            +'</div>'
                            +'<dir-pagination-controls max-size="3" direction-links="true" boundary-links="true" > </dir-pagination-controls>';
    var details_stock_template='<div class="card">'
                                +    '<div class="container-fliud">'
                                +       ' <div class="wrapper row">'
                                +            '<div class="preview col-md-6">'
                                +                '<div class="action">'
                                +                    '<h2 class="product-title">{{vm.info[1]}}</h2>'
                                +                    '<h4 class="product-title">{{vm.items[0].modelo.modelo}}-{{vm.items[0].modelo.ms_name}}</h4>'
                                +                    '<button class="like btn btn-default" type="button"><span class="fa fa-heart"></span></button>'
                                +                '</div>'
                                +                '<div class="preview-pic tab-content">'
                                +                '<div class="tab-pane active" id="pic-1">'
                                +                    '<img ng-src="{{vm.items[0].url}}">'
                                +                '</div>'
                                +               ' </div>'
                                +            '</div>'
                                +            '<div class="details col-md-6">'
                                +                '<h3>Especificaciones tecnicas:</h3>'
                                +                '<div class="product-description" >'
                                +                    '<p>Productor: {{vm.items[0].modelo.bando}}</p>'
                                +                    '<p>Inicio de su prodccion: {{vm.items[0].modelo.production_date}}</p>'
                                +                    '<p>Tiempo del encendido: {{vm.items[0].modelo.encendido}}ms</p>'
                                +                    '<p>Energia Total: {{vm.items[0].modelo.energia}}mha</p>'
                                +                    '<p>Peso Total: {{vm.items[0].modelo.peso_Total}}kg</p>'
                                +                    '<p>Peso Vacio: {{vm.items[0].modelo.peso_Vacio}}kg</p>'
                                +                    '<p>Pilotos: {{vm.items[0].modelo.pilots}}</p>'
                                +                    '<p>Rango de los sensores opticos:{{vm.items[0].modelo.rango_Sensor}}m</p>'
                                +                    '<p>Velocidad Maxima: {{vm.items[0].modelo.velocidad}}km/h</p>'
                                +                '</div>'
                                +                '<h4 class="price">Price per unit: {{vm.items[0].modelo.custom_price}}€</h4>'
                                +                '<div class="center input-group">'
                                +                    '<span class="input-group-btn">'
                                +                       ' <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">'
                                +                            '<span class="glyphicon glyphicon-minus"></span>'
                                +                        '</button>'
                                +                   ' </span>'
                                +                    '<input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="10">'
                                +                    '<span class="input-group-btn">'
                                +                        '<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">'
                                +                            '<span class="glyphicon glyphicon-plus"></span>'
                                +                        '</button>'
                                +                   ' </span>'
                                +                '</div>'
                                +                '<button class="add-to-cart btn btn-default" type="button">add to cart</button>'
                                +            '</div>'
                                +        '</div>'
                                +    '</div>'
                                +'</div>';
    var all_catalog_template='<form class="form-inline">'
                            +    '<div class="form-group">'       
                            +        '<label >Search</label>'
                            +        '<input type="text" ng-model="search" class="form-control" placeholder="Search">'
                            +    '</div>'
                            +'</form>'
                            +'<div class="flex-container">'
                            +    '<div  dir-paginate="products in vm.items|filter:search|itemsPerPage:3">'
                            +        '<div class="flex-item ">'
                            +           '<a class="product_tittle">{{products.modelo.ms_name}}</a>'
                            +            '<img class="product_image" ng-src="{{products.url}}">'
                            +            '<a href="#/details:{{products.modelo.ms_name}}"><i class="fa fa-eye"></i></a>'
                            +            '<add-To-Stock data="products.modelo" ></add-To-Stock>'/* <i class="fa fa-shopping-cart"></i>' */
                            +        '</div>'
                            +    '</div>'
                            +'</div>'
                            +'<dir-pagination-controls max-size="3" direction-links="true" boundary-links="true" > </dir-pagination-controls>';
    var details_catalog_template='<div class="card">'
                                +    '<div class="container-fliud">'
                                +       ' <div class="wrapper row">'
                                +            '<div class="preview col-md-6">'
                                +                '<div class="action">'
                                +                    '<h3 class="product-title">{{vm.items[0].modelo.modelo}}{{vm.items[0].modelo.ms_name}}</h3>'
                                +                    '<button class="like btn btn-default" type="button"><span class="fa fa-heart"></span></button>'
                                +                    '<add-To-Stock data="vm.items[0].modelo" ></add-To-Stock>'
                                +                '</div>'
                                +                '<div class="preview-pic tab-content">'
                                +                '<div class="tab-pane active" id="pic-1">'
                                +                    '<img ng-src="{{vm.items[0].url}}">'
                                +                '</div>'
                                +               ' </div>'
                                +            '</div>'
                                +            '<div class="details col-md-6">'
                                +                '<h3>Especificaciones tecnicas:</h3>'
                                +                '<div class="product-description" >'
                                +                    '<p>Productor: {{vm.items[0].modelo.bando}}</p>'
                                +                    '<p>Inicio de su prodccion: {{vm.items[0].modelo.production_date}}</p>'
                                +                    '<p>Tiempo del encendido: {{vm.items[0].modelo.encendido}}ms</p>'
                                +                    '<p>Energia Total: {{vm.items[0].modelo.energia}}mha</p>'
                                +                    '<p>Peso Total: {{vm.items[0].modelo.peso_Total}}kg</p>'
                                +                    '<p>Peso Vacio: {{vm.items[0].modelo.peso_Vacio}}kg</p>'
                                +                    '<p>Pilotos: {{vm.items[0].modelo.pilots}}</p>'
                                +                    '<p>Rango de los sensores opticos:{{vm.items[0].modelo.rango_Sensor}}m</p>'
                                +                    '<p>Velocidad Maxima: {{vm.items[0].modelo.velocidad}}km/h</p>'
                                +                '</div>'
                                +                '<h4 class="price">Price per unit: {{vm.items[0].modelo.price_per_unit}}€</h4>'
                                +                '<div class="center input-group">'
                                +                    '<span class="input-group-btn">'
                                +                       ' <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">'
                                +                            '<span class="glyphicon glyphicon-minus"></span>'
                                +                        '</button>'
                                +                   ' </span>'
                                +                    '<input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="10">'
                                +                    '<span class="input-group-btn">'
                                +                        '<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">'
                                +                            '<span class="glyphicon glyphicon-plus"></span>'
                                +                        '</button>'
                                +                   ' </span>'
                                +                '</div>'
                                /* +                '<button class="add-to-cart btn btn-default" type="button">add to cart</button>' */
                                +            '</div>'
                                +        '</div>'
                                +    '</div>'
                                +'</div>';



    function template(controller){
        var pre_template="";
        switch (controller) {
        case "catalog":
            pre_template =  all_catalog_template;
            break;
        case "details":
            pre_template=details_catalog_template;
            break;
        case "shop_st":
            /* console.log("hola"); */
            pre_template=all_stock_template;
            break;
        case "shop_dt":
            console.log("hola");
            pre_template=details_stock_template;
            break;
            
        }
        return pre_template;
    }; 
     var linker = function(scope, element, attrs) {
        console.log(scope.vm);
        element.html(template(scope.vm.filter.split(" ")[1],scope)).show();
        $compile(element.contents())(scope);
    }
      return {
          restrict: 'E', //Default for 1.3+
          scope: {
            filter: "@filter"
          },
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
         /*  template: all_catalog_template , */
         link: linker,
      };

}]);