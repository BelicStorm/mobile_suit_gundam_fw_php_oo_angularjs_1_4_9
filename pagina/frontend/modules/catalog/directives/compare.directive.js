gundam_proyect_root.directive("test",["dialog","$compile","$rootScope",function(dialog,$compile,$rootScope){
    var controller = function () {
        var vm = this;
        console.log($rootScope.compare_items);
        if($rootScope.compare_items){
            vm.producto1=$rootScope.compare_items.item1.product1.modelo;
            vm.producto2=$rootScope.compare_items.item2.product2.modelo;
        }
        vm.addItem = function () {
            /* console.log(vm.id); */
            $rootScope.compare_items=vm.id;
           dialog.showAdvanced('<test type="details" id="vm.id"></test>');
        };
      };
      var template_button = '<button ng-click="vm.addItem()">Compare</button>';
      var template_details= '<table class="table-fill">'
                            +    '<thead>'
                            +       ' <tr>'
                            +            '<th class="text-left"></th>'
                            +            '<th class="text-left">{{vm.producto1.user_name}}:{{vm.producto1.modelo}}-{{vm.producto1.ms_name}}</th>'
                            +            '<th class="text-left">{{vm.producto2.user_name}}:{{vm.producto2.modelo}}-{{vm.producto2.ms_name}}</th>'
                            +        '</tr>'
                            +    '</thead>'
                            +    '<tbody class="table-hover">'
                             +        '<tr>'
                            +           '<td class="text-left">Peso Total</td>'
                            +           '<td class="text-left">{{vm.producto1.peso_Total}}</td><td class="text-left">{{vm.producto2.peso_Total}}</td>'
                            +       ' </tr>'
                             +        '<tr>'
                            +           '<td class="text-left">Peso Vacio</td>'
                            +           '<td class="text-left">{{vm.producto1.peso_Vacio}}</td><td class="text-left">{{vm.producto2.peso_Vacio}}</td>'
                            +       ' </tr>'
                             +        '<tr>'
                            +           '<td class="text-left">Velocidad</td>'
                            +           '<td class="text-left">{{vm.producto1.velocidad}}</td><td class="text-left">{{vm.producto2.velocidad}}</td>'
                            +       ' </tr>'
                             +        '<tr>'
                            +           '<td class="text-left">Energia</td>'
                            +           '<td class="text-left">{{vm.producto1.energia}}</td><td class="text-left">{{vm.producto2.energia}}</td>'
                            +       ' </tr>'
                             +        '<tr>'
                            +           '<td class="text-left">Encendido</td>'
                            +           '<td class="text-left">{{vm.producto1.encendido}}</td><td class="text-left">{{vm.producto2.encendido}}</td>'
                            +       ' </tr>'
                             +        '<tr>'
                            +           '<td class="text-left">Rango del Sensor</td>'
                            +           '<td class="text-left">{{vm.producto1.rango_Sensor}}</td><td class="text-left">{{vm.producto2.rango_Sensor}}</td>'
                            +       ' </tr>'
                             +        '<tr>'
                            +           '<td class="text-left">Pilotos</td>'
                            +           '<td class="text-left">{{vm.producto1.pilots}}</td><td class="text-left">{{vm.producto2.pilots}}</td>'
                            +       ' </tr>'
                             +        '<tr>'
                            +           '<td class="text-left">Precio por unidad</td>'
                            +           '<td class="text-left">{{vm.producto1.custom_price}}</td><td class="text-left">{{vm.producto2.custom_price}}</td>'
                            +       ' </tr>'
                             +        '<tr>'
                            +           '<td class="text-left">Fecha de produccion</td>'
                            +           '<td class="text-left">{{vm.producto1.production_date}}</td><td class="text-left">{{vm.producto2.production_date}}</td>'
                            +       ' </tr>'
                            +    '</tbody>'
                            +'</table>';    
      function template(type){
            var pre_template="";
            switch (type) {
                case "button":
                    pre_template =  template_button;
                    break;
                case "details":
                    pre_template=  template_details;
                    break; 
                }
            return pre_template;
        };
         var linker = function(scope, element, attrs) {
            console.log(scope.vm);
            element.html(template(scope.vm.type)).show();
            $compile(element.contents())(scope);
        }   
      return {
          restrict: 'E', //Default for 1.3+
          scope: {
              id: '=id',
              type: '@',
          },
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
          /* template: template */
          link: linker
      };

}]);

 

    
    