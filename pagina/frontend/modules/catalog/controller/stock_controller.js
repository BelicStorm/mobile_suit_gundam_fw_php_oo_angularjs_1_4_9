gundam_proyect_root.controller("stock_controller",stock_main_controller);

function stock_main_controller($route,services,stock_services,data_storage,toaster){
    var stock_variable = this;
    stock_variable.stock="";
    stock_variable.price="";
    
    data_storage.get_user("token").then(function(name){ //print the stock of the loged shop
        var data={data:{token:name,option:"shop"},action:"select_stock"};
        stock_services.stock(data).then(function(resolve){
            console.log(resolve);
            stock_variable.print=resolve;
        })
    })

    stock_variable.modificate_stock= function(param,product_name) { //manage the stock of the loged shop
        data={data:"no"};
        data_storage.get_user("token").then(function(name){ //print the stock of the loged shop
            switch (param) {
                case "del_s":
                    /* console.log("deleting from data base"); */
                    data={action:"modificate_stock_table",data:{to_do:"delete",product:{token:name,name:product_name}}};
                    break;
                case "up_p":
                    /* console.log(stock_variable.price); */
                    data={action:"modificate_stock_table",data:{to_do:"price",product:{token:name,name:product_name,new_price:stock_variable.price}}};
                    break;
                case "up_s":
                    /* console.log(stock_variable.stock); */
                    data={action:"modificate_stock_table",data:{to_do:"stock",product:{token:name,name:product_name,new_stock:stock_variable.stock}}};
                    break;
                case "both":
                    /* console.log(stock_variable.stock);
                    console.log(stock_variable.price); */
                    data={action:"modificate_stock_table",data:{to_do:"both",product:{token:name,name:product_name,new_price:stock_variable.price,new_stock:stock_variable.stock}}};
                    break;
            
                default:
                console.log("Unsuported operation");
                    break;
            }
            if(data.data!="no"){
                /* console.log(data); */
                services.post("stock_shop","stock",data).then(function(resolve){
                    /* console.log(resolve); */
                    $route.reload();
                    toaster.toast(resolve);
                }) 
            }
           
        })
        
    }
    
}