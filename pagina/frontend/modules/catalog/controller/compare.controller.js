gundam_proyect_root.controller("compare_controller",compare_controller);
function compare_controller(){
    var compare_scope= this;
    compare_scope.starter=false;
    compare_scope.selected1 = {};
    compare_scope.selected2 = {};
     compare_scope.select = function(item) {
        if (compare_scope.selected1.product1 == undefined ) {
            compare_scope.selected1 = {product1:item};
            console.log( compare_scope.selected1.product1);

            return;
        }
        
        // second select
        if (!compare_scope.selected2.product2) {
            if (compare_scope.selected1.product1==item) {
                compare_scope.selected1 = {};
                return;
            }else {
                compare_scope.selected2 = {product2:item};
                compare_scope.compare_container = {item1:compare_scope.selected1,item2:compare_scope.selected2};
                /* console.log(compare_scope.compare_container); */
                return;
            }
        }
         // back to first select again
        compare_scope.selected1 = {product1:item};
        compare_scope.selected2 = {};
        compare_scope.compare_container = {};
     }
}