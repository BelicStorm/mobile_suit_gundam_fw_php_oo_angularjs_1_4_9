gundam_proyect_root.factory("stock_services",  ['services','$http','$q', function (services,$http, $q) {
    var obj = {};
    
    obj.stock = function (param) {
        return get_stock(param,services,$http, $q);
    };
    return obj;
}]);
function http_get_from_json($http, $q){
    var url="http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_AngularJS_1_4_9/pagina/frontend/media/ms_images.json?callback=JSON_CALLBACK";
    var defered=$q.defer();
    var promise=defered.promise;
    $http({
            method: "GET" ,
            url: url
        }).success(function(data, status, headers, config) {
            defered.resolve(data);
        }).error(function(data, status, headers, config) {
            defered.reject(data);
        });
    return promise;
}
function get_stock(param,services,$http, $q){
    var defered=$q.defer();
    var promise=defered.promise;
    var data="";
    var product_preview=[];
    services.post('stock_shop','stock',param).then(function(resolve) {
            data= resolve;
           /*  console.log(data); */
        http_get_from_json($http, $q).then(function(img) {
            /* console.log(img); */
            angular.forEach(data, function(value1, key1) {
                /* console.log(value1); */
                angular.forEach(img, function(value2, key2) {
                    /* console.log(value2); */
                        angular.forEach(value2, function(value3, key3) {
                            if (value3.id==value1.ms_name) {
                                    /* console.log(value3.url);  */
                                product_preview.push({modelo:value1,url:value3.url});
                            }
                        });
                });
            });
            /*  console.log(product_preview); */
            defered.resolve(product_preview);
        })
    }); 
     return promise;
}