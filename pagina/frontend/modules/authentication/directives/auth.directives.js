gundam_proyect_root.directive("defaultImageSet",["$http","$compile","$q",function($http,$compile,$q){
    var controller = function () {
        var vm = this;
        function get_pre_profile_pics() { 
            var defered=$q.defer();
            var promise=defered.promise;
            $http({
                    method: "GET" ,
                    url: "http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_angularjs_1_4_9/pagina/frontend/media/ms_images.json"
                }).success(function(data, status, headers, config) {
                    defered.resolve(data['profile_pics']);
                }).error(function(data, status, headers, config) {
                    defered.reject(data);
                });
            return promise;
        }
        function init() { 
            get_pre_profile_pics().then(function(data) {
                vm.items=data;
                /* console.log(vm.items); */
            })
        }
        
        init();

    };
    var test= '<div id="profile_pic"  ng-repeat="i in vm.items">'
              +  '<img ng-src={{i.url}}>'
              +   '<input type="radio" ng-model="vm.valThroughEquals" name="user_pic" value={{i.url}}>'    
              +  '</div>';
             

     var linker = function(scope, element, attrs) {
        console.log(scope.vm);
        element.html(test).show();
        $compile(element.contents())(scope);
    }
      return {
          restrict: 'E', //Default for 1.3+
          scope: {
             valThroughEquals: '='
          },
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
         /*  template: all_catalog_template , */
         link: linker,
      };

}]);
gundam_proyect_root.directive("thirdTab",["data_storage","$compile","$q",function(data_storage,$compile,$q){
  return {
    template: '<ng-include src="getTemplateUrl()"/>',
    scope: {
        user: '=data'
    },
    restrict: 'E',
    controller: function($scope) {
      //function used on the ng-include to resolve the template
      data_storage.get_user("type").then(function(type){
          $scope.type=type;
      })
      $scope.getTemplateUrl = function() {
        //basic handling
            switch ($scope.type) {
                case "2":
                    return "frontend/modules/authentication/views/third_tab_normalUser.view.html";
                case "1":
                    return "frontend/modules/authentication/views/third_tab_adminUser.view.html";
                case "3":
                    return "frontend/modules/authentication/views/third_tab_shopUser.view.html";
            
                default:
                    break;
            }
        
      }
    }
  };
}]);

  