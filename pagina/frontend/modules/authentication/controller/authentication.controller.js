gundam_proyect_root.controller("auth_controller", auth_controller);

function auth_controller(services,data_storage,$window){
    var auth_controller = this;
    auth_controller.form = {
                        inputName: "",
                        inputEmail: "",
                        inputForgEmail: "",
                        inputPass: "",
                        avatar:""
                    };
    auth_controller.change_form = {inputpass1:""};

    data_storage.get_user("all").then(function(resolve){
        auth_controller.user_profile_data={
                        user_name:resolve["user_name"],
                        user_avatar:resolve["user_avatar"]
        }
    })
    auth_controller.sing_up = function(){
        /* console.log(auth_controller.form); */
        var data = {action:"register_user",data:auth_controller.form};
        services.post('authentication','auth',data).then(function(resolve) {
            console.log(resolve);
            if(resolve=='"no"'){
               auth_controller.error_register="The user already exists";
            }else{
                $window.location.reload();
            }
                      
        })
    }
    auth_controller.forgot_pass=function(){
        console.log(auth_controller.form);
        var data = {action:"forgot_password",data:auth_controller.form.inputForgEmail};
        services.post('authentication','auth',data).then(function(resolve) {
            /* console.log(resolve); */
            if(resolve=='"no"'){
               auth_controller.error_forgot="The user doesnt exists";
            }else{
                 $window.location.href="#/";
                 $window.location.reload();
            }
        })
    }
    auth_controller.sing_in=function(){
        console.log(auth_controller.form);
         var data = {action:"login",data:auth_controller.form};
        services.post('authentication','auth',data).then(function(resolve) {
            /* console.log(resolve); */
            if(resolve=='"no"'){
                auth_controller.login_error="The user or mail doesn't exist";
            }else{
                data_storage.put_all_user_data(resolve);
                auth_controller.login_error="";
                $window.location.href="#/";
                 $window.location.reload();
            }
        })
    }
    auth_controller.change_pass=function(){
        /* console.log(auth_controller.change_form); */
        data_storage.get_user("token").then(function(token){//obtiene el token
             var data={action:"update", data:{
                                                to_update:"password",
                                                thing:auth_controller.change_form.inputpass1,
                                                token:token}};//genera el array para el servidor
           /*  console.log(data); */
            services.post('authentication','auth',data).then(function(resolve) {//envia los datos para que actualicen la contraseña
                console.log(resolve);
                if(resolve=='"no"'){//si hay un error
                    auth_controller.change_pass_error="The password cant be the same as the old";
                }else{
                    data_storage.put_specific_user_data("token",resolve[0]["user_token"]);//guarda el nuevo token
                    $window.location.reload();//recarga la pagina
                }
            }) 
        });
  
    }
    auth_controller.profile_third_tab_action=function(){
        
    }
}