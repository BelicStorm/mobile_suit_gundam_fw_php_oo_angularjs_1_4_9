gundam_proyect_root.controller("contact_controller", contact_controller);

function contact_controller(services,$window){
    var contact_controller = this;
    contact_controller.contacto = {
                        inputName: "",
                        inputEmail: "",
                        inputSubject: ""
                    };
    contact_controller.SubmitContact = function(){
        /* console.log(contact_controller.contact); */
        var data = {"inputName": contact_controller.contacto.inputName, "inputEmail": contact_controller.contacto.inputEmail, 
        "inputSubject": contact_controller.contacto.inputSubject,"token":'contact_form'};
        var contact_form = JSON.stringify(data);
        services.post("contact","send_cont",contact_form).then(function (response) {
            if(response=="ok"){
                $window.location.href = '#/';
            }
        });
    }
}