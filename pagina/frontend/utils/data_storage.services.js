gundam_proyect_root.factory("data_storage",  ['services','$window','$q', function (services,$window, $q) {
    var obj = {};
    obj.put_all_user_data = function (param) {
        $window.localStorage.setItem("user_type",param["type"]);
        $window.localStorage.setItem("user_name",param["name"]);
        $window.localStorage.setItem("user_avatar",param["avatar"]);
        $window.localStorage.setItem("user_token",param["token"]);
        /* console.log($window.localStorage); */
    };
    obj.put_specific_user_data = function (condition,element) {
        return put_user_info(condition,element,$q,$window);
    };
    obj.destroy_users_sesion = function () {
        $window.localStorage.clear();
    };
    obj.get_user = function (condition) {
         return get_user_info(condition,$q,$window);
    };
    
    return obj;
}]);
function get_user_info(condition,$q,$window){
    var defered=$q.defer();
    var promise=defered.promise;
    var return_data={};
    switch (condition) {
        case "all":
            return_data={
                 user_name:$window.localStorage.getItem("user_name"),
                 user_type:$window.localStorage.getItem("user_type"),
                 user_avatar:$window.localStorage.getItem("user_avatar")
            }
            defered.resolve(return_data);
            break;
        case "token":
            return_data=$window.localStorage.getItem("user_token");
            defered.resolve(return_data);
            break;
        case "type":
            return_data=$window.localStorage.getItem("user_type");
            defered.resolve(return_data);
            break;
        case "name":
            return_data=$window.localStorage.getItem("user_name");
            defered.resolve(return_data);
            break;
    
        default:
            defered.reject(data);
            break;
    }
    /* console.log(return_data); */
    return promise;
}
function put_user_info(condition,element,$q,$window){
    switch (condition) {
        case "token":
            $window.localStorage.setItem("user_token",element);
            break;
        case "avatar":
            $window.localStorage.setItem("user_avatar",element);
            break;
    
        default:
            break;
    }
}