gundam_proyect_root.factory("toaster", ["$mdToast","$document",function( $mdToast, $document){
      var obj = {};
    obj.toast = function (text) {
       var ctrl = this;
       var last = {
            bottom: true,
            top: false,
            left: false,
            right: true
        };
        ctrl.toastPosition = angular.extend({}, last);

        ctrl.getToastPosition = function() {
            sanitizePosition();
            return Object.keys(ctrl.toastPosition)
                .filter(function(pos) {
                    return ctrl.toastPosition[pos];
                }).join(' ');
        };

        function sanitizePosition() {
            var current = ctrl.toastPosition;
            if (current.bottom && last.top) {
                current.top = false;
            }
            if (current.top && last.bottom) {
                current.bottom = false;
            }
            if (current.right && last.left) {
                current.left = false;
            }
            if (current.left && last.right) {
                current.right = false;
            }
            last = angular.extend({}, current);
        }
            var pinTo = ctrl.getToastPosition();
            var toast = $mdToast.simple()
                        .textContent(text)
                        .actionKey('z')
                        .actionHint('Press the Control-"z" key combination to ')
                        .action('Close')
                        .dismissHint('Activate the Escape key to dismiss this toast.')
                        .highlightAction(true)
                        // Accent is used by default, this just demonstrates the usage.
                        .highlightClass('md-accent')
                        .position(pinTo)
                        .hideDelay(0);
            $mdToast.show(toast);
     
    };
     
    return obj;
}]);