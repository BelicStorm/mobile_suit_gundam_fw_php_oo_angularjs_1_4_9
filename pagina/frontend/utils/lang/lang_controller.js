gundam_proyect_root.controller('lang_controller', function ($scope, $rootScope, $translate) {
    $scope.changeLanguage = function (key) {
        console.log(key);
        localStorage.setItem("lang",key);
        $rootScope.lang = localStorage.getItem("lang");
        $translate.use($rootScope.lang);
    };
});