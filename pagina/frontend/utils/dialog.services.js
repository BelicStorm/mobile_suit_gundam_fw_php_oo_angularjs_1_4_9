gundam_proyect_root.factory('dialog', ["$mdDialog","$document",function($mdDialog, $document){
    var vm=this;
    vm.status = '  ';
    vm.customFullscreen = false;
    var obj={};

    obj.showBarDialog=function(tittle,context) { //Modificar cuando se requiera el uso XD
        $mdDialog.show(
        $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title(tittle)
            .textContent(context)
            .ariaLabel('Alert Dialog Demo')
            .ok('Got it!')
        );
    };
    obj.showConfirm = function() {//Modificar cuando se requiera el uso XD
     // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
          .title('Would you like to delete your debt?')
          .textContent('All of the banks have agreed to forgive you your debts.')
          .ariaLabel('Lucky day')
          .ok('Please do it!')
          .cancel('Sounds like a scam');

        $mdDialog.show(confirm).then(function() {
            vm.status = 'You decided to get rid of your debt.';
        }, function() {
            vm.status = 'You decided to keep your debt.';
        });
    };
    obj.showPrompt = function() {//Modificar cuando se requiera el uso XD
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.prompt()
            .title('What would you name your dog?')
            .textContent('Bowser is a common name.')
            .placeholder('Dog name')
            .ariaLabel('Dog name')
            .initialValue('Buddy')
            .required(true)
            .ok('Okay!')
            .cancel('I\'m a cat person');

        $mdDialog.show(confirm).then(function(result) {
            vm.status = 'You decided to name your dog ' + result + '.';
        }, function() {
            vm.status = 'You didn\'t name your dog.';
        });
    };
    obj.showAdvanced = function(context) {
        console.log(context);
        $mdDialog.show({
            controller: DialogController,
            template: context,
            parent: angular.element(document.body),
            /* targetEvent: ev, */
            clickOutsideToClose:true,
            fullscreen: vm.customFullscreen // Only for -xs, -sm breakpoints.
        })
        /* .then(function(answer) {
            $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
            $scope.status = 'You cancelled the dialog.';
        }); */
    };
    obj.showTabDialog = function() {//Modificar cuando se requiera el uso XD
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'tabDialog.html',
            parent: angular.element(document.body),
           /*  targetEvent: ev, */
            clickOutsideToClose:true
        })
           /*  .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            }); */
    };

    function DialogController($mdDialog) {
        vm.hide = function() {
            $mdDialog.hide();
        };

        vm.cancel = function() {
            $mdDialog.cancel();
        };

        vm.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }
    return obj;
}]);