gundam_proyect_root.directive('whenScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];
        
        elm.bind('scroll', function() {
            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.whenScrolled);
            }
        });
    };
});
gundam_proyect_root.directive('menu',["$compile","data_storage","$q",function($compile,data_storage,$q){
    var vm = this;
    var controller = function () {
        function init() { 
            vm.type="";
            vm.name="";
            vm.avatar="";
        }
        init();
    };
    
    function template(){
        var pre_template="";
        var defered=$q.defer();
        var promise=defered.promise;
        data_storage.get_user("all").then(function(resolve){
            vm.type=resolve["user_type"];
            vm.name=resolve["user_name"];
            vm.avatar=resolve["user_avatar"];
            ///templates
                var defaul_tmenu_first= +'<div class="header-bottom">'
                                +    '<div class="w3ls-logo">'
                                +       '<h1>Anaheim Electronics</h1>'
                                +    '</div>'
                                +    '<div class="header-top-right">'
                                +        '<a href="#/login">Login</a>'
                                +        '<div id="cart">'
                                +           ' <i class="fa fa-shopping-cart" ></i>'
                                +            '<div id="ex4"></span>'
                                +           ' </div>'
                                +       ' </div>'
                                +   ' </div>'
                                +    '<div class="clearfix"> </div>'
                                +'</div>'
                                +'<div class="top-nav">'
                                + '<nav class="navbar navbar-default">'
                                +    ' <div class="container">'
                                +        ' <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu'						
                                +         '</button>'
                                +     '</div>'
                                +       '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">'
                                +           '<ul  class="nav navbar-nav" id="menu_handler">';	
                var loged_menu_first= +'<div class="header-bottom">'
                                        +    '<div class="w3ls-logo">'
                                        +       '<h1>Anaheim Electronics</h1>'
                                        +    '</div>'
                                        +    '<div class="header-top-right">'
                                        +       '<div id="user_avatar"><a href="#/profile"><img id="avatar" src="'+vm.avatar+'"> <span id="user_name">'+vm.name+'</span></div></a>'
                                        +        '<a href="#/logout">Logout</a>'
                                        +        '<div id="cart">'
                                        +           ' <i class="fa fa-shopping-cart" ></i>'
                                        +            '<div id="ex4"></span>'
                                        +           ' </div>'
                                        +       ' </div>'
                                        +   ' </div>'
                                        +    '<div class="clearfix"> </div>'
                                        +'</div>'
                                        +'<div class="top-nav">'
                                        + '<nav class="navbar navbar-default">'
                                        +    ' <div class="container">'
                                        +        ' <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu'						
                                        +         '</button>'
                                        +     '</div>'
                                        +       '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">'
                                        +           '<ul  class="nav navbar-nav" id="menu_handler">';				
                var menu_end=                       '</ul>'
                                        +       '</div>'		
                                        +    '</nav>'
                                        +'</div>';

                var default_menu_content='<li class="home-icon"><a ><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>'
                                +'<li><a href="#/">Inicio</a></li>'
                                +'<li><a href="#/contact">Contact Us</a></li>'
                                +'<li><a href="#/">Simple Promise</a></li>'
                                +'<li><a href="#/">Test de scopes</a></li>';
                var normal_menu_content='<li class="home-icon"><a ><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>'
                                +'<li><a href="#/">Inicio</a></li>'
                                +'<li><a href="#/shop_st:all">Shop</a></li>'
                                +'<li><a href="#/contact">Contact Us</a></li>';
                var shop_menu_content='<li class="home-icon"><a ><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>'
                                +'<li><a href="#/">Inicio</a></li>'
                                +'<li><a href="#/catalog:all">Catalogo</a></li>'
                                +'<li><a href="#/contact">Contact Us</a></li>';
                var admin_menu_content='<li class="home-icon"><a ><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>'
                                +'<li><a href="#/">Inicio</a></li>'
                                +'<li><a href="#/catalog:all">Catalogo</a></li>'
                                +'<li><a href="#/contact">Contact Us</a></li>'
                                +'<li><a href="#/">Simple Promise</a></li>'
                                +'<li><a href="#/">Test de scopes</a></li>';
            
            ///templates
            ///create the template
                switch (vm.type) {
                    case "1":
                        pre_template=loged_menu_first+admin_menu_content+menu_end;
                        defered.resolve(pre_template);
                        break;
                    case "2":
                        pre_template=loged_menu_first+normal_menu_content+menu_end;
                        defered.resolve(pre_template);
                        break;
                    case "3":
                        pre_template=loged_menu_first+shop_menu_content+menu_end;
                        defered.resolve(pre_template);
                        break;
                    default:
                        pre_template=defaul_tmenu_first+default_menu_content+menu_end;
                        defered.resolve(pre_template);
                        break;    
                }
            ///create the template
        })
        return promise;
        
    }; 
    var linker = function(scope, element, attrs) {
        template().then(function(resolve){
            /* console.log(resolve); */
            element.html(resolve).show();
            $compile(element.contents())(scope);
        })
    }
      return {
          restrict: 'EA', //Default for 1.3+
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
         link: linker,
      };

}]);
gundam_proyect_root.directive('slideForShops',["$compile","data_storage","$q",function($compile,data_storage,$q){
    var vm = this;
    var controller = function () {
        function init() { 
            vm.type="";
        }
        init();
    };
    
    function template(){
        var pre_template="";
        var defered=$q.defer();
        var promise=defered.promise;
        data_storage.get_user("type").then(function(resolve){
            if(resolve!=null){
                vm.type=resolve;
               if(vm.type=="3" || vm.type=="1"){
                   pre_template= '<div class="slides">'
                               +    ' <div class="slide active blue" >'
                               +         '<div class="text-slide">'
                               +             '<h2>{{ "CATALOG_TITLE" | translate}}</h2>'
                               +             '<p>{{ "CATALOG_SUBTITLE" | translate}}</p>'
                               +           '<a  class="offer all" ng-click="home_main_controller.all_catalog()" >{{ "CATALOG_ALL" | translate}}</a>'
                               +             '<a  class="offer zeon" ng-click="home_main_controller.zeon_catalog()" >{{ "CATALOG_ZEON" | translate}}</a>'
                               +             '<a  class="offer eff"  ng-click="home_main_controller.eff_catalog()">{{ "CATALOG_EFF" | translate}}</a>'
                               +         '</div>'
                               +     '</div>'
                               + '</div>';
                     defered.resolve(pre_template);
               }else{ 
                   pre_template= '<div class="slides">'
                               +    ' <div class="slide active blue" >'
                               +         '<div class="text-slide">'
                               +             '<h2>{{ "CATALOG_TITLE" | translate}}</h2>'
                               +             '<p>{{ "CATALOG_SUBTITLE" | translate}}</p>'
                               +           '<a  class="offer all" href="#/shop_st:all" >{{ "CATALOG_ALL" | translate}}</a>'
                               +         '</div>'
                               +     '</div>'
                               + '</div>';
                   defered.resolve(pre_template);
                }
                ///create the template
            }else{ defered.resolve(pre_template);}
        })
        return promise;
        
    }; 
    var linker = function(scope, element, attrs) {
        template().then(function(resolve){
            /* console.log(resolve); */
            element.html(resolve).show();
            $compile(element.contents())(scope);
        })
    }
      return {
          restrict: 'EA', //Default for 1.3+
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
         link: linker,
      };

}]);
