gundam_proyect_root.controller('AppController',function(services,data_storage,$window){
        var ctrl = this;
        ctrl.data = { upload:[] }  // <= upload data get pushed here 
        ctrl.upload= function(ubication){
            console.log(ubication.split("@"));
            var ubication=ubication.split("@");
            data_storage.get_user("token").then(function(token){
                var data = {action:"upload_image",data:{to_upload:ctrl.data,token:token}};
                services.post(ubication[0],ubication[1],data).then(function(resolve){
                    /* console.log(resolve); */
                    if (resolve!='"no"') {
                        console.log("resolve");
                        switch (resolve["return"]) {
                            case "update_avatar":
                                data_storage.put_specific_user_data("token",resolve["data"]["token"]);
                                data_storage.put_specific_user_data("avatar",resolve["data"]["avatar"]);
                                $window.location.reload();
                                break;
                        
                            default:
                                break;
                        }
                        /* $window.location.reload(); */
                    
                    }else{
                        ctrl.error="An error has occurred or an image was not placed";
                    }
                })
            })
        }
        ctrl.delete= function(){
            ctrl.data = { upload:[] };
            
        }
    }
);
       



gundam_proyect_root.directive('dropZone',[
        function(){
            var config = {
                template:'<label class="drop-zone">'+
                         '<input type="file"  accept="jpg,png" />'+
                         '<div ng-transclude></div>'+       // <= transcluded stuff
                         '</label>',
                transclude:true,
                replace: true,
                require: '?ngModel',
                link: function(scope, element, attributes, ngModel){
                    var upload = element[0].querySelector('input');    
                        upload.addEventListener('dragover', uploadDragOver, false);
                        upload.addEventListener('drop', uploadFileSelect, false);
                        upload.addEventListener('change', uploadFileSelect, false);                
                        config.scope = scope;                
                        config.model = ngModel; 
                }
            }
            return config;


            // Helper functions
            function uploadDragOver(e) { e.stopPropagation(); e.preventDefault(); e.dataTransfer.dropEffect = 'copy'; }
            function uploadFileSelect(e) {
                 console.log(config.scope.app.data.upload);
   
                if (config.scope.app.data.upload.length+1==1) {
                    /* console.log("Hola"+config.scope.app.data.upload.length); */
                     e.stopPropagation();
                    e.preventDefault();
                    var files = e.dataTransfer ? e.dataTransfer.files: e.target.files;
                    for (var i = 0, file; file = files[i]; ++i) {
                        /* console.log(file); */
                        var reader = new FileReader();
                        reader.onload = (function(file) {
                            return function(e) { 
                                // Data handling (just a basic example):
                                // [object File] produces an empty object on the model
                                // why we copy the properties to an object containing
                                // the Filereader base64 data from e.target.result
                                var data={
                                    data:e.target.result,
                                    dataSize: e.target.result.length
                                };
                                for(var p in file){ data[p] = file[p] }
                                
                                config.scope.$apply(function(){ config.model.$viewValue.push(data) })
                            }
                        })(file);
                        reader.readAsDataURL(file);
                    }
                }else{console.log("Solo puedes subir una foto.")}
               
            }
        }
    ])