gundam_proyect_root.factory("services", ['$http','$q', function ($http, $q) {
    var serviceBase = '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_AngularJS_1_4_9/pagina/backend/index.php?module=';
    var obj = {};

        obj.get = function (module, functi) {
            var url=serviceBase + module + '&function=' + functi;
            var method='GET';
            return http_1(method,url,$http,$q);
        };

        obj.get2 = function (module, functi, dada) {
            var method= 'GET';
            var url= serviceBase + module + '&function=' + functi + '&aux=' + dada;
            return http_1(method,url,$http,$q);
        };

        obj.get3 = function (module, functi, dada, dada2) {
            var method= 'GET';
            var url= serviceBase + module + '&function=' + functi + '&aux=' + dada + '&aux=' + dada2;
            return http_1(method,url,$http,$q);
        };

        obj.post = function (module, functi, dada) {
            /* console.log(module);console.log(functi);console.log(dada); */
            var method= 'POST';
            var url= serviceBase + module + '&function=' + functi;
            var data= dada;
            return  http_2(method,url,data,$http,$q);
        };

        obj.put = function (module, functi, dada) {
            var method= 'PUT';
            var url= serviceBase + module + '&function=' + functi;
            var data= dada;
            return  http_2(method,url,data,$http,$q);
        };

        obj.delete = function (module, functi, dada) {
            var method= 'DELETE';
            var url= serviceBase + module + '&function=' + functi + '&aux=' + dada;
            return http_1(method,url,$http,$q);
        };
        
    return obj;
}]);
function http_1(method,url,$http,$q){
    var defered=$q.defer();
    var promise=defered.promise;
    $http({
            method: method ,
            url: url
        }).success(function(data, status, headers, config) {
            defered.resolve(data);
        }).error(function(data, status, headers, config) {
            defered.reject(data);
        });
    return promise;

}
function http_2(method,url,data,$http,$q){
    var defered=$q.defer();
    var promise=defered.promise;
    $http({
            method: method ,
            url: url,
            data: data
        }).success(function(data, status, headers, config) {
            defered.resolve(data);
        }).error(function(data, status, headers, config) {
            defered.reject(data);
        });
        /* console.log(data); */
    return promise;

}