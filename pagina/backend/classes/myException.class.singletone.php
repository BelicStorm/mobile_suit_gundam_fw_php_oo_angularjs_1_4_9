<?php
class myException extends Exception { 
       
    function get_Message() { 
          $menssage=$this->getMessage();
          $code=$this->getCode();
          $file=$this->getFile();
          $at_line=$this->getLine();
          $with_trace=$this->getTraceAsString();
          $msg = "Exception has ocurred: $menssage".
                    "Exception Code: [$code]".
                    "File affected: $file".
                    "At line: $at_line".
                    "With trace: $with_trace";
        $this->save_in_log($msg);
          return $msg; 
    }
    function get_Message_for_user(){
        $menssage=$this->getMessage();
          $code=$this->getCode();
          $file=$this->getFile();
          $at_line=$this->getLine();
          $with_trace=$this->getTraceAsString();
          $User_msg = "An error has ocurred. Please contact with the administrators";
          $msg="Exception has ocurred: $menssage". 
                    "Exception Code: [$code]".
                    "File affected: $file".
                    "At line: $at_line".
                    "With trace: $with_trace";
                  
        $this->save_in_log($msg);
        return $User_msg;
    }
    function save_in_log($msg) { 
          $log = Log::getInstance();
          $log->addLine($msg);
    }  

} 
