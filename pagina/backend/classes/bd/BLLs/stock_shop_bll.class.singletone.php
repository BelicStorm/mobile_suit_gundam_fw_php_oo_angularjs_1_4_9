<?php
class stock_shop_bll extends core_bll { 
    static $_instance;

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function add_to_stock($data){
        $token=$data["token"];
        $ms_id=$data["modelo_id"];
        $name=$data["name"];
        $price=$data["current_price"];
        $id=$this->common->get_user_token($token,"")[0]["user_id"];
        $stock=$this->common->get_stock_table_with_where($id,$ms_id,$name);
        if($stock){
            $new_stock_added=$stock[0]["stock"]+1;
            $this->common->update_current_stock_number($id,$name,$new_stock_added);
            return "Actual stock of ".$name." $new_stock_added";
        }else{
            $values="'$id'".",".$ms_id.","."'$name'".","."1".",".$price;
            $this->dao->insert_into("stock_shop_table");
            $this->dao->values_insert_into($values);
            $this->dao->put($this->db, $this->dao->content);
            return $name." added to stock";
        }
        
    }
    public function select_stock($data){
        switch ($data["option"]) {
            case 'shop':
                $id=$this->common->get_user_token($data["token"],"")[0]["user_id"];
                $stock=$this->common->get_stock_specific_shop($id);
                return $stock;
                break;
            
            case 'all':
                /* var_dump("all stock"); */
               $this->dao->content="CALL get_stock(null,null)";//llamaremos a un procedimiento almacenado que inserte el user
            return  $this->dao->get($this->db, $this->dao->content);//y lo ejecutaremos
            case 'one':
                /* var_dump("all stock"); */
               $this->dao->content="CALL get_stock('".$data["shop_name"]."','".$data["item"]."')";//llamaremos a un procedimiento almacenado que inserte el user
            return  $this->dao->get($this->db, $this->dao->content);//y lo ejecutaremos
               
               
        }
    }
    public function modificate_stock_table($data){
        $id=$this->common->get_user_token($data["product"]["token"],"")[0]["user_id"];
        switch ($data["to_do"]) {
            case 'stock':
                /* var_dump($data["product"]); */
                $this->common->update_current_stock_number($id,$data["product"]["name"],$data["product"]["new_stock"]);
                return 'Stock modified';
            case 'delete':
                /* var_dump($data["product"]); */
                $this->common->delete_product($id,$data["product"]["name"]);
                 return 'Item Removed';
            case 'price':
                $this->common->update_current_price($id,$data["product"]["name"],$data["product"]["new_price"]);
                return 'Price modified';
            case 'both':
                /* var_dump($data["product"]); */
                $this->common->update_current_price($id,$data["product"]["name"],$data["product"]["new_price"]);
                $this->common->update_current_stock_number($id,$data["product"]["name"],$data["product"]["new_stock"]);
               return 'Product modified';
            
            default:
                # code...
                break;
        }
    }
   

}