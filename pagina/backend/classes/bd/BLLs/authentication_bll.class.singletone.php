<?php
class authentication_bll extends core_bll { 
   
    static $_instance;

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function register_user($info_new_user) {
        /* var_dump("en el bll"); */
       $id=$info_new_user['inputEmail']."_".$info_new_user['inputName'];//generar la id de los users logeados de forma convencional
        $mail=$this->common->get_user_mail($info_new_user['inputEmail'],$id);//verificar que el mail no existe en la bd
        if(empty($mail)){//si el mail no existe entonces insertara el usuario
             $info_new_user["token"]=generate_user_id_token($info_new_user['inputName']);//generara un token inicial para su verificacion
             $sql="CALL create_user('".$id
                                    ."','".$info_new_user['inputName']
                                    ."','".$info_new_user['avatar']
                                    ."','".$info_new_user['inputEmail']
                                    ."','".$info_new_user['inputPass']
                                    ."','".$info_new_user['token']
                                    ."',0)";//llamaremos a un procedimiento almacenado que inserte el user
           $this->db->ejecutar($sql);//y lo ejecutaremos
           $mail=array('type'=>'alta',
                        "inputEmail"=>$info_new_user['inputEmail'],
                        "inputMessage"=>'Para activar tu cuenta pulse en el siguiente enlace',
                        "token"=>$info_new_user['token']); 
             enviar_email($mail);//enviaremos un mail al usuario con su direccion para asi poder verificar el registro
             
            return "ok";
        }else{
            return "no";
        };
    }
    public function validate_register($old_Token){
        /* var_dump($old_Token); */
        $token=$this->common->get_user_token($old_Token,"");// obtener el id con el antiguo token
        
        $array=array(0=>array("column"=>"user_activated","argument"=>"1"));//generar el array del update
        $this->common->simple_update("user_control","user_id='".$token[0]["user_id"]."'",$array);//activar el usuario
        $this->dao->put($this->db, $this->dao->content);

        $new_token=generate_user_id_token($token[0]["user_id"]);//generar un nuevo token
        $this->common->update_token($old_Token,$new_token,"");//actualizar el token
    }
    public function forgot_password($mail){
         $mail_response=$this->common->get_user_mail($mail,"");
         /* var_dump($mail_response); */
         if(empty($mail_response)){
             return "no";
         }else{
             $new_temp_pass=generate_random_string();
             $old_Token=$this->common->get_user_token_by_id($mail_response[0]["user_id"]);
             $new_token=generate_user_id_token("forgot_pass1");
             $this->common->update_token($old_Token[0]["user_token"],$new_token,"");
             $mail=array('type'=>'changepass',
                            "inputEmail"=>$mail,
                            "inputMessage"=>'Recomendamos que cambie su contraseña al iniciar sesion.'
                            .' Nueva contraseña: '.$new_temp_pass ,
                              "token"=>$new_token,
                            "new_pass"=>$new_temp_pass); 
             enviar_email($mail);
             return "ok";
         }
    }
    public function login($info_login){
        $mail_response=$this->common->get_user_mail($info_login["inputEmail"],"");
        /*  var_dump($mail_response); */
         if(empty($mail_response)){
             return "no";
         }else{
             $password_response=$this->common->get_user_password($mail_response[0]["user_id"]);
              /* var_dump($password_response); */
              if ($password_response[0]["user_password"]!=$info_login["inputPass"]) {
                  return "no";
              }else{
                  $data=$this->common->get_all_info($mail_response[0]["user_id"])[0];
                  return array("name"=>$data["user_name"],"avatar"=>$data["user_avatar"],
                               "token"=>$data["user_token"],"type"=>user_type($data)); 
              }
         }
    }
    public function upload_image($image){//actualizar avatar
      /*   var_dump($image["token"]); */
        if (!$image["to_upload"]["upload"]) {
           return "no";
        }else{
            $url=Upload_files_ROOT."user_avatar";//url de la carpeta de los avatares
            $path=upload_image::upload_picture($url,$image["to_upload"]["upload"][0]["data"],$image["to_upload"]["upload"][0]["name"]);//subir la imagen y generar su path
            $id=$this->common->get_user_token($image["token"],"")[0];//obtener el id mediante el token
            $this->common->update_avatar($id["user_id"],$path);//actualizar avatar y el token
            $new_token=$this->common->get_user_token_by_id($id["user_id"])[0];//obtiene el nuevo token
            $data=array("return"=>"update_avatar","data"=>array("avatar"=>$path,"token"=>$new_token["user_token"])); 
            return $data;
        }
    }
    public function update($component){
        /* var_dump($component);
        var_dump($this->common->get_user_token($component["token"],"")); */
        switch ($component["to_update"]) {
            case 'password':
                $id=$this->common->get_user_token($component["token"],"")[0];
                $old_pass=$this->common->get_user_password($id["user_id"])[0]["user_password"];
                if($old_pass!=$component["thing"]){
                    $this->common->update_password($id["user_id"],$component["thing"]);
                    $new_token=$this->common->get_user_token_by_id($id["user_id"]);
                    return $new_token;
                }else{
                    return "no";
                }
                break;
        }
    }
}
function user_type($user_info){
   $user_type=explode("@", $user_info["user_mail"]);//obtienes el identificador de tipo
      if ($user_type[0]!="admin") {//si es un usuario corriente
        if ($user_type[0]!="tienda") { //si es un usuario comun
          $type="2";
        }else{ $type="3";}//una tienda
      } else { $type="1";}//un administrador
      return $type;
}
function generate_random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}