<?php
class home_bll extends core_bll { 
    static $_instance;

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function list_products(){
        $this->dao->select("modelo.nombre as Modelo,
                            mobile_suit.ms_name as Nombre,
                            bandos.nombre as Productor","mobile_suit");
        $this->dao->join_argument("modelo");
        $this->dao->on_argument("mobile_suit.ms_modelo_id = modelo.modelo_id");
        $this->dao->join_argument("bandos");
        $this->dao->on_argument("modelo.bando=bandos.bando_id");
        return $this->dao->get($this->db, $this->dao->content);
    }
   

}