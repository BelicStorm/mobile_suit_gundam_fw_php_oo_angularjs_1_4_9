<?php
class catalog_bll extends core_bll { 
    static $_instance;

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function get_products($argument){
        /* var_dump("en el bll: ".$argument); */
        
        $this->dao->select(" bandos.bando_id,
                      modelo.modelo_id,
                      bandos.nombre as bando,
                      modelo.nombre as modelo, 
                      ms_name,tamaño,
                      tamaño_Total,
                      peso_Total,peso_Vacio,
                      velocidad,energia,encendido,
                      rango_Sensor,pilots,price_per_unit,
                      production_date","mobile_suit");
        $this->dao->join_argument("modelo");
        $this->dao->on_argument(" mobile_suit.ms_modelo_id = modelo.modelo_id");
        $this->dao->join_argument("bandos");
        $this->dao->on_argument("modelo.bando=bandos.bando_id");

        if ($argument!="all") {
             $this->dao->where_argument(self::build_where_argument($argument));
        }
       /*  var_dump($this->dao->content); */
        return $this->dao->get($this->db, $this->dao->content);
    }
   private function build_where_argument($argument){
       switch ($argument) {
           case 'zeon':
               return "bandos.bando_id=1";
               break;
           
           case 'eff':
               return "bandos.bando_id=2";
               break;
           default:
               return "ms_name='".$argument."'";
               break;
       }
   }
   

}