<?php
class common_sentence { 
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = DAO::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function select_all($tabla) {
        $arrArgument=$this->dao->select("*","$tabla");
    }
    public function select_with_where($tabla,$where) {
         $arrArgument=$this->dao->select("*","$tabla");
         $arrArgument=$this->dao->where_argument("$where");
    }
    public function simple_update($tabla,$where,$array) {
         $this->dao->update($tabla);
         foreach ($array as $key => $value) {
             if($key==0){
                 $this->dao->update_set($value["column"],$value["argument"]);
             }else{
                 $this->dao->more_update_set($value["column"],$value["argument"]);
             }
             
         }
         $this->dao->where_argument($where);
        /*  var_dump( $this->dao->content); */
        }
    public function simple_delete($tabla,$where){
        $this->dao->delete($tabla);
        $this->dao->where_argument($where);
    }
    /* $array=array(1=>array("column"=>,"argument"=>)...); */
    
    //common stock sentences
        public function get_stock_table_with_where($id,$modelo,$name){
            $where= "user_id='$id' and ms_modelo_id=$modelo and ms_name='$name'";
            self::select_with_where("stock_shop_table",$where);
            /* var_dump($this->dao->content); */
            return $this->dao->get($this->db, $this->dao->content);
        }
        public function update_current_stock_number($id,$name,$stock){
            $array=array(0=>array("column"=>"stock","argument"=>$stock));
            $where= "user_id='$id' and ms_name='$name'";
            self::simple_update("stock_shop_table",$where,$array);
            /* var_dump($this->dao->content); */
            return $this->dao->put($this->db, $this->dao->content);
        }
        public function update_current_price($id,$name,$price){
            $array=array(0=>array("column"=>"custom_price","argument"=>$price));
            $where= "user_id='$id' and ms_name='$name'";
            self::simple_update("stock_shop_table",$where,$array);
            /* var_dump($this->dao->content); */
            return $this->dao->put($this->db, $this->dao->content);
        }
        public function delete_product($id,$name){
            $where= "user_id='$id' and ms_name='$name'";
            self::simple_delete("stock_shop_table",$where);
            /* var_dump($this->dao->content); */
            return $this->dao->put($this->db, $this->dao->content);
        }
        public function get_stock_specific_shop($id){
            $where= "user_id='$id'";
            self::select_with_where("stock_shop_table",$where);
            /* var_dump($this->dao->content); */
            return $this->dao->get($this->db, $this->dao->content);
        }
    //common stock sentences

    //common user sentences
        public function get_all_info($id){
            $this->dao->content = "CALL get_user('".$id."')"; 
            
              return  $this->dao->get($this->db,$this->dao->content);
        }
        public function get_user_mail($mail,$id){
            self::select_with_where("user_mail","user_mail='".$mail."' ");
            return $this->dao->get($this->db, $this->dao->content);
        }
        public function get_user_token($token,$id){
            self::select_with_where("user_token","user_token='".$token."'");
            return $this->dao->get($this->db, $this->dao->content);
        }
        public function get_user_token_by_id($id){
            self::select_with_where("user_token","user_id='".$id."'");
            return $this->dao->get($this->db, $this->dao->content);
        }
        public function get_user_avatar($id){
            self::select_with_where("user_avatar","user_id='".$id."' ");
            return $this->dao->get($this->db, $this->dao->content);
        }
        public function get_user_verificate($id){
            self::select_with_where("user_validation","user_id='".$id."'");
            return $this->dao->put($this->db, $this->dao->content);
        }
        public function get_user_password($id){
            self::select_with_where("user_password","user_id='".$id."'");
            return $this->dao->get($this->db, $this->dao->content);
        }
    //common user sentences
    //common update user sentences
        public function update_token($token,$new_token){
            $array=array(0=>array("column"=>"user_token","argument"=>"$new_token"));       
            self::simple_update("user_token","user_token='$token'",$array);
            return $this->dao->put($this->db, $this->dao->content);
        }
        public function update_password($id,$user_password){
            $array=array(0=>array("column"=>"user_password","argument"=>"$user_password"));       
            self::simple_update("user_password","user_id='$id'",$array);
            $this->dao->put($this->db, $this->dao->content);

            $new_token=generate_user_id_token($user_password);
            $array=array(0=>array("column"=>"user_token","argument"=>"$new_token"));     
            self::simple_update("user_token","user_id='$id'",$array);
            return $this->dao->put($this->db, $this->dao->content);
        }
        public function update_avatar($id,$user_avatar){
            $array=array(0=>array("column"=>"user_avatar","argument"=>"$user_avatar"));       
            self::simple_update("user_avatar","user_id='$id'",$array);
            $this->dao->put($this->db, $this->dao->content);

            $new_token=generate_user_id_token($user_avatar);
            $array=array(0=>array("column"=>"user_token","argument"=>"$new_token"));     
            self::simple_update("user_token","user_id='$id'",$array);
            return $this->dao->put($this->db, $this->dao->content);
        }
    //common update user sentences
}