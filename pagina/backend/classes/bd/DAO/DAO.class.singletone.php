<?php
    class DAO {
        public $content="";
        static $_instance;

            private function __construct() {

            }
            public static function getInstance() {
                if(!(self::$_instance instanceof self)){
                    self::$_instance = new self();
                }
                return self::$_instance;
            }
    //Pure Statements
            public function select($rows,$tabla){
                $this->content= "SELECT $rows from $tabla";
            }
            public function insert_into($tabla){
                $this->content= "INSERT INTO $tabla";
            }
            public function delete($tabla){
                $this->content= "DELETE FROM $tabla";
            }
            //insert into options
                public function colums_insert_into($rows){
                    $this->content=$this->content . "($rows)";
                }
                public function values_insert_into($values){
                    $this->content=$this->content . " VALUES ($values)";
                }
                public function more_values_insert_into($values){
                    $this->content=$this->content . ",($values)";
                }
            //insert into options
            public function update($table){
                $this->content="UPDATE $table SET ";
            }
            //update options
                public function update_set($column,$argument){
                    $this->content=$this->content ." $column = '$argument'";
                }
                public function more_update_set($column,$argument){
                    $this->content=$this->content ." ,$column = '$argument'";
                }
            //update options
    //Pure Statements
    //Optional statement options
            public function join_argument($table){
                $this->content= $this->content . " inner join " . $table;
            }
            public function on_argument($argument){
                 $this->content= $this->content . " on " . $argument;
            }
            public function where_argument($argument){
                $this->content= $this->content . " where " . $argument;
            }
            public function and_argument($argument){
                 $this->content= $this->content . " and " . $argument;
            }
            public function or_argument($argument){
                 $this->content= $this->content . " or " . $argument;
            }
            public function order_argument($by,$form){
                 $this->content= $this->content . " order by " . $by . " " . $form;
            }
            public function limit_argument($number){
                 $this->content= $this->content . " limit " . $number;
            }
            public function sub_select($rows,$tabla){
                $this->content= $this->content . " SELECT $rows from $tabla ";
            }
    //Optional statement options
            //ejecutar sentencias
            public static function get($db){
                return $db->listar($db->ejecutar(self::$_instance->content));
            }
            public static function post($db, $arrArgument){
                
            }
            public static function put($db){
                return $db->ejecutar(self::$_instance->content);
            }
            //ejecutar sentencias
    }