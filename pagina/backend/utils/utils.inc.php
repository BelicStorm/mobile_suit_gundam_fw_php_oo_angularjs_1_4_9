<?php
function amigable($url, $return = false) {
    $amigableson = URL_AMIGABLES;
    $link = "";
    if ($amigableson) {
        $url = explode("&", str_replace("?", "", $url));
        foreach ($url as $key => $value) {
            $aux = explode("=", $value);
            if (!$aux){
                $link .=  $aux[1]."/";
            }else {
                $link .= "/". $aux[1];
            }
        }
    } else {
        $link = "index.php" . $url;
    }
    if ($return) {
        return Amigable_PATH . $link;
    }
    echo Amigable_PATH . $link;
}
///mail functions
    function enviar_email($arr) {    
        $structure = call_user_func($arr['type'].'_mail',$arr);
        
        //set_error_handler('ErrorHandler');
        try{
            /* if ($arr['type'] === 'admin') */
                $address = 'cpardoca3@gmail.com';
            /* else
                $address = $arr['inputEmail']; */

            $result = send_mailgun('cpardoca3@gmail.com', $address, $structure["subject"], $structure["html"]);    
        } catch (Exception $e) {
            $return = 0;
        }
        //restore_error_handler();
        return $result;
    }
    function contact_mail($message){
        $subject = 'Tu Petici&oacute;n  ha sido enviada<br>';
        $ruta = '<a href=' . 'http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_angularjs_1_4_9/pagina/#/'. '>aqu&iacute;</a>';
        $body = 'Para visitar nuestra web, pulsa ' . $ruta;
        $structure = array(
            "subject" => $subject,
            "html" => build_mail_structure($subject,$body,$message['inputMessage'])
        );
        
        return $structure;
    }
    function admin_mail($message){
        $subject = $message['inputSubject'];
        $body = 'inputName: ' . $message['inputName']. '<br>' .
        'inputEmail: ' . $message['inputEmail']. '<br>' .
        'inputSubject: ' . $message['inputSubject']. '<br>' .
        'inputMessage: ' . $message['inputMessage'];
        $structure = array(
            "subject" => $subject,
            "html" => build_mail_structure($subject,$body,$message['inputMessage'])
        );
        
        return $structure;
    }
    function changepass_mail($message){
        $subject = 'Tu Nuevo Password.<br>';   
        $ruta = "<a href='http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_angularjs_1_4_9/pagina/#/update/:password/:".$message["new_pass"]."/:" . $message['token'] . "'>aqu&iacute;</a>";

        $body = 'Al pulsar el enlace cambiara su contraseña: ' . $ruta;
        $structure = array(
            "subject" => $subject,
            "html" => build_mail_structure($subject,$body,$message['inputMessage'])
        );
        
        return $structure;
    }
    function alta_mail($message){
        $subject = 'Tu Alta:';
        $ruta = "<a href='http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_angularjs_1_4_9/pagina/#/verificate:" . $message['token'] . "'>aqu&iacute;</a>";
        $body = 'Gracias por unirte a nuestra aplicaci&oacute;n<br> Para finalizar el registro, pulsa ' . $ruta;
        $structure = array(
            "subject" => $subject,
            "html" => build_mail_structure($subject,$body,$message['inputMessage'])
        );
        
        return $structure;
    }
    function build_mail_structure($subject,$body,$message){
        $html="";
        $html .= "<html>";
            $html .= "<body>";
                $html .= "Asunto:";
                $html .= "<br><br>";
            $html .= "<h4>". $subject ."</h4>";
            $html .= "<br><br>";
            $html .= "Mensaje:";
            $html .= "<br><br>";
            $html .= $message;
            $html .= "<br><br>";
            $html .= $body;
            $html .= "<br><br>";
            $html .= "<p>Sent by Anaheim Electronics</p>";
            $html .= "</body>";
            $html .= "</html>";
        return $html;
    }
    function send_mailgun($from, $email, $subject, $html){
        $config = array();
    	$config['api_key'] = "c9a6f18a51c10174ec01f466e1a3cf12-6140bac2-f8adc7b4"; //API Key
    	$config['api_url'] = "https://api.mailgun.net/v3/sandbox7edb3ced1abd46c3974719a51c170f64.mailgun.org/messages"; //API Base URL

    	$message = array();
    	$message['from'] = $from;
    	$message['to'] = $email;
    	$message['h:Reply-To'] = "cpardoca3@gmail.com";
    	$message['subject'] = $subject;
        $message['html'] = $html;
     
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $config['api_url']);
    	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    	curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    	curl_setopt($ch, CURLOPT_POST, true); 
    	curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
    	$result = curl_exec($ch);
    	curl_close($ch);
    	return $result;
 }
///mail functions

///token functions
    function generate_user_id_token($user){
        $header = '{"typ":"JWT", "alg":"HS256"}';
        ///////////////////////////////////////////////////////////////////yomogan
        //iat: Tiempo que inició el token
        //exp: Tiempo que expirará el token (+1 hora)
        //name: info user
        $payload = "{
        'iat':time(), 
        'exp':time() + (60*60),
        'name': $user
        }";
        $JWT = new JWT;
        $token = $JWT->encode($header, $payload, secret_JWT);
        $json = $JWT->decode($token, secret_JWT);
        return $token;
    /*  echo 'JWT yomogan: '.$token."\n\n"; echo '<br>';
        echo 'JWT Decoded yomogan: '.$json."\n\n"; echo '<br>'; echo '<br>'; */
    }
///token functions

function Redirect($url, $permanent = false)
{
    header('Location: ' . $url, true, $permanent ? 301 : 302);

    exit();
}

