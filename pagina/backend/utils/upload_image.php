<?php
class upload_image {
    public static function upload_picture($Folder,$image,$name){//mover la imagen de temp al directorio especificado
        if(!empty($image)){
        if (preg_match('/^data:image\/(\w+);base64,/',$image , $type)) {
                $image = substr($image, strpos($image, ',') + 1);
                $type = strtolower($type[1]); // jpg, png, gif

                if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                    throw new \Exception('invalid image type');
                }

                $image = base64_decode($image);

                if ($image === false) {
                    throw new \Exception('base64_decode failed');
                }
            } else {
                throw new \Exception('did not match data URI with image data');
            }
            file_put_contents($Folder."/".$name, $image);
            return Uploaded_Avatars.$name;
        }else{
            return "no";
        }
       /* var_dump($Folder); */
      /*  return $Folder; */
    }
}

