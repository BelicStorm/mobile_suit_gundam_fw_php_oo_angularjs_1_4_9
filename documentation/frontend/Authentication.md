[Index](index.md)
# **Authentication**
---
Este módulo es el encargado de gestionar las vistas de las funcionalidades de: Registro, Login y el perfil de los usuarios.
## **Register**
Una característica notable de este módulo es el uso de una directiva para seleccionar un avatar en el registro:

| Uso en el html| Directiva |
| ------| ------ |
| ![html](../media/Register_default_imageset_html.PNG)| ![directiva](../media/Register_default_imageset_directive.PNG) |

## **Profile**
El perfil cuenta con varias características a destacar las cuales son una directiva encargada de materializar el Dropzone y otra encargada de facilitarle un tab propio a cada tipo de usuario.

### **DropZone**:
La directiva del dropzone cuenta con un controlador, que es el encargado de capturar las ordenes de carga, borrado y subida de la imagen. Tras esto, la directiva, solo carga la vista del dropzone, los listeners y crea un array donde almacenar la imagen en base64.
(Esta Funcionalidad dispone de Test)

### **Third Tab**:
Esta directiva se encarga de cargar de forma dinámica la vista de la tercera funcionalidad del perfil de cada tipo de usuario. El tipo de usuario es pasado como parámetro a la directiva. (El uso de directivas parametrizadas cuenta con un test)

| Uso en el html| Directiva |
| ------| ------ |
| ![html](../media/Third_tab_html.PNG)| ![directiva](../media/Third_tab_directive.PNG) |

#### ***Atencion***
El tercer tab que unicamente supone un cambio en la funcionalidad de la pagina web es el de los usuarios de tipo tienda el cual se encarga de manejar las alteraciones que las tiendas hacen sobre sus productos:

| | | |
| ------| ------ | ----- |
| ![0](../media/tab0.PNG)| ![1](../media/tab1.PNG) | ![2](../media/tab2.PNG)|
--- 