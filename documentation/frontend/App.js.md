[Index](index.md)
# **app.js**

Encargado de enrutar todos los movimientos en el frontend y asignar los diferentes controladores a las vistas. También es el encargado de cargar el idioma cuando la página arranca.
##     1. Ejemplo de Enrutamiento: 
![Enrutamiento](../media/App.js_router_example.PNG)
##     2. Carga del lenguaje desde app.js:
-    Cuando la aplicación arranca esta, directamente, crea un ámbito root con el lenguaje a pasarle al controlador, pero si es la primera vez que se accede o se accede con una sesión nueva este pondrá el inglés como lenguaje por defecto:

![Translate](../media/App.js_run_example.PNG)

- Luego el $translateProvider se encargara de cargar las traducciones ubicadas en ficheros externos .json:

![Translate](../media/App.js_translate_example.PNG)    
---