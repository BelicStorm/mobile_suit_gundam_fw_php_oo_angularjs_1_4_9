[Index](index.md)
# **Home**
---
    Modulo principal de la aplicación. Únicamente tiene como función contener el scroll infinito. El scroll infinito funciona mediante una directiva, parametrizada con el ámbito del controlador que llama a la carga de datos, que se activa cuando se hace scroll en el div contenedor
    
| Uso en el html| Directiva |
| ---- | ---- |
| ![html](../media/Home_scroll_html.PNG)| ![directiva](../media/Home_scroll_directive.PNG) |
---