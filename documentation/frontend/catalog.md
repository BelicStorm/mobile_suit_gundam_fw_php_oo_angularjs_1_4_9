[Index](index.md)
# **Catalog**
El catalogo es modulo encargado de cargar los productos, de diferentes formas, para los usuarios y brindarles la oportunidad de comparar los productos de las tiendas.

***Funcionamiento:***
El catalogo para usuarios y tiendas está regido por dos directivas que formatean los datos dependiendo de qué vista se desee (Grid o Details) y cuál de los dos usuarios este usando el catalogo (Tienda o cliente). En el caso de las tiendas todo depende de la directiva catalog.directive. En el caso de un usuario cliente, catalog.directive, se encarga de formatear la vista de los productos y la directiva stock.catalog será la encargada de cargar el stock de las tiendas. El encargado de indicar que vista es formateada es el controlador de stock o catálogo.
- Ejemplo de funcionamiento:
    - En el html colocaremos la directiva encargada de imprimir la vista, a la cual se le pasara por parámetro los ámbitos del controlador de catalogo ( o de stock). El ámbito llamado filtro es el encargado de indicar al servidor que datos se requieren y el ámbito llamado controller de imprimir los datos en grid o details:

         ![html](../media/catalog_example0.PNG)
    ---
    - En los controladores (controlador de catálogo) iniciaremos los ámbitos anteriormente comentados, los cuales dependerán de los parámetros de la ruta:

         ![html](../media/catalog_example_1.PNG)
    ---
    - La directiva será creada de la siguiente forma, haciendo posible que con los ámbitos podamos seleccionar que datos queremos (función init() ) y mas tarde de que manera los queremos en la vista (función template())

         ![html](../media/catalog_example2.PNG)
    ---
    - Por ultimo remarcar que la vista en details del controlador se accede desde el mismo, como vemos en la zona remarcada.

         ![html](../media/catalog_example4.PNG)
    ---  

## **Stock Mode**
En este modo los usuarios ingresados como tienda pueden explotar la funcionalidad de añadir los productos listados por el catalogo a su Stock personal, el que luego será imprimido por la directiva de catálogo cuando los usuarios cliente ingresen en la vista del catálogo.

***Funcionamiento:***
El funcionamiento está ligado a la directiva de catálogo ya que esta directiva alberga en la impresión de sus vistas la directiva para añadir al stock:

- Ejemplo de funcionamiento:
    - La última línea de la zona remarcada es la directiva del stock. A esta solo le pasamos como parámetro los datos del producto seleccionado:
        ![html](../media/catalog_example4.PNG)

    - La directiva será la encargada de enviar los datos al servidor y este a su vez añadir a la base de datos el producto a la tabla de stock:
        ![html](../media/stock_example0.PNG)

    - A raíz de esta acción se abren dos nuevas funcionalidades: Alter Product(Tiendas) y el Compare Mode(Cliente).
        - [Alter Product](Authentication.md)
        - [Compare Product](#Compare-Mode)

## **Compare Mode**

El modo de comparado es una funcionalidad íntegramente del usuario cliente, el cual puede comparar en vivo las características de dos artículos del stock visible:

![ejemplo_compare](../media/ejemplo_compare.PNG)

***Funcionamiento:***
El funcionamiento de la directiva depende íntegramente de los datos y las vistas imprimidas de las directivas de Catalogo y Stock.
    
-  En la vista en details, que obtendremos de la directiva de catalogo, proporcionada a los usuarios se añadirán nuevas funcionalidades:

    |**View**|
    | ---- |
    |![compare_view](../media/stock_example_compare0.PNG)
    |En la primera línea remarcada le indicamos cual será el controlador encargado de manejar los productos a comparar.  |
    El checkbox de la segunda línea remarcada maneja un ámbito, del controlador del compare mode, que se encarga mostrar u ocultar los botones para añadir al array de comparaciones.|
    Las tres últimas líneas remarcadas se encargan de añadir al array de comparaciones los productos seleccionados.|
    Por último, la tercera línea remarcada se encarga de pasar los datos a la directiva de comparaciones y mostrar por pantalla la consecuente comparación. Esta solo se activa si en el controlador de comparaciones el segundo ítem está lleno.| 
    |

    |**Controller**|
    | ---- |
    | ![compare_controler](../media/compare_controller.PNG) |
     Cuando en la vista uno de los productos es pulsado pasa por el controlador. Cuando los dos productos son pulsados estos se añaden a un array el cual será enviado a la directiva, en caso de que se pulse el botón de comparar |
    | 

    |**Directive**|
    | ---- |
    | ![compare_directive](../media/Compare_directive.PNG) |
     La primera parte de la directiva se encarga de mostrar la visa del botón de comparar. Cuando este es pulsado pasa consigo el array creado en el controlador y se lanza un MaterialDialog con la directiva, como parámetro de vista, y los datos de los productos, los cuales son guardados en una variable $RouteScope.|
     La segunda parte de la directiva se encarga de mostrar el MaterialDialog con la directiva ya formateada con los datos de los productos. Para ello cuando esta es llamada averiguara si la variable $routeScope.compare_items existe y posteriormente, en la función template() se creara la tabla que se vera en el dialogo.| 
     |
