[Index](index.md)

(Estas funcionalidades cuentan con su respectivo [Test](../../README.md#TESTS))

# Logs
Esta funcionalidad se encarga de capturar los errores y las excepciones de la aplicación.

Esta funcionalidad tiene dos clases: la clase Log y la clase myException.

## **Log Class**
---
La clase de Log es la encargada de escribir en los ficheros de log los errores acaecidos en la aplicación.
|||
|---|---|
|<td rowspan=4>![html](../media/logs0.PNG)    |    1. La clase se construye sobre un path y un file name estático. 
|                                             |    2. El método _save se encarga de abrir el fichero y guardar en el los datos recibidos por la clase. 
|                                             |    3. El método addline() se encarga de formatear la frase que se añadirá cuando ocurra el error. 
|                                             |    4. En esta línea se añadirá la fecha en la que ha ocurrido.|

 ## **Exception Handler Class**
 ---
 La clase myException, la cual extiende de Exception, es la encargada de modificar el texto resultante en una excepción ocurrida durante un funcionamiento anómalo de nuestra aplicación.
 La clase diferencia dos tipos de mensajes, los cuales serán los que el administrador vera, y los que se guardaran en el log, y los que el usuario común observara. 
 Tras generar el texto de excepción, myException, llamara a la clase Log para guardar el mensaje.

 ***Funcionamiento:***

Para poner en funcionamiento la clase tendremos que poner en el código que el capturador de Excepciones ha cambiado y que mensajes queremos guardar en el log:

    try {
        if (file_exists($model)) {
            $modelClass = $model_name;
            if (!method_exists($modelClass, $function)){
                throw new myException("The Method sended doesn't exist");
            }
            $obj = $modelClass::getInstance();
            if (isset($arrArgument)){
                return $obj->$function($arrArgument);
            }
        } else {
            throw new myException("The file doesn't exist");
        }
    } catch (myException $e) {
        echo $e->get_Message();
    }

Tanto la función get_Message como get_Message_for_user se encargan de recolectar los datos que la clase Exception genera para poder formatearlos a nuestro gusto:

    function get_Message() { 
          $menssage=$this->getMessage();
          $code=$this->getCode();
          $file=$this->getFile();
          $at_line=$this->getLine();
          $with_trace=$this->getTraceAsString();
          $msg = "Exception has ocurred: $menssage".
                    "Exception Code: [$code]".
                    "File affected: $file".
                    "At line: $at_line".
                    "With trace: $with_trace";
        $this->save_in_log($msg);
          return $msg; 
    }

Por último la función, save_in_log, se encarga llamar al método de la clase Log encargado de añadir la línea al fichero de logs:

    function save_in_log($msg) { 
          $log = Log::getInstance();
          $log->addLine($msg);
    }  

##  **Error Handler Library**
---

Al igual que la clase de captura de excepciones, esta librería de funciones se encarga de capturar los errores acaecidos en el funcionamiento de la aplicación y, según su código de error y su nombre, formatea un mensaje el cual vera el usuario y se escribirá en el log.

***Funcionamiento:***

El nombre del error nos escribirá el principio de la línea de error:

    function errorHandler($errno, $errstr, $errfile, $errline) {
        $msg="";
        switch ($errno) { 
            case E_NOTICE:
            case E_USER_NOTICE:
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
            case E_STRICT:
                $msg="Error: [$errno] STRICT error $errstr at $errfile:$errline n";
                break;
    
            case E_WARNING:
            case E_USER_WARNING:
                $msg="Error: [$errno] WARNING error $errstr at $errfile:$errline n";
                break;
    
            case E_ERROR:
            case E_USER_ERROR:
            case E_RECOVERABLE_ERROR:
                $msg="Error: [$errno] FATAL error $errstr at $errfile:$errline n";
                break;
            default:
                $msg="Error: [$errno] Unknown error at $errfile:$errline n";
                break;
        }

En este momento de genera la última parte de la línea de error, la cual nos indica el estado de la conexión tras el error:

        $code=http_response_code();
        $msg=$msg." Code: ".$code." Message: ".error_list_code($code); 
        print_error($msg);

Y al final llamamos a la función que añadirá la línea al fichero de log:

        $log = Log::getInstance();
        $log->addLine($msg);
    }

Este capturardor se activa de la siguiente forma:

    set_error_handler("errorHandler");
        $action=$_POST["action"];
        $data=$_POST["data"];
        $return=load_bll("authentication_bll",$action,$data);
    restore_error_handler();

##  **Sintaxis de los Logs**

***Error***

15-05-2019 06:30:26: Error: [8] STRICT error Undefined index: email at (Line) n Code: 200 Message: OK

***Exception***

15-05-2019 07:43:42: Exception has ocurred: The Method sended doesn't existException Code: [0]File affected: (Line & File)
