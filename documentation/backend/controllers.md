[Index](index.md)

# Controllers

Los controladores siguen una sintaxis parecida, ya que su unica funcion en la aplicacion es la de redirigir las peticiones a les BLL.
Exceptuando el controlador de contacto ya que este tiene que encargarse de enviar, posteriormente, el mensaje generado.

***Sintaxis***

El nombre de la clase tiene que ser el mismo que tiene el fichero y el mismo que tiene que estar guardado en el allowed_modules, para que sea cargada por el autoload y descubierta por el Router:

    class authentication  {

El nombre del metodo tiene que ser el mismo que se encuentra en el fichero allowed_functions para que el router pueda encontrarlo:

        public static function auth(){

Añadiremos la siguiente linea para que los datos enviados desde el frontend sean legibles por la clase:

            $_POST = json_decode(file_get_contents('php://input'), true);

La sintaxis para la recepcion de datos es de un objeto que almacenara que metodo del bll va a efectuarse (action) y los datos a pasarle a este (data):

                set_error_handler("errorHandler");
                    $action=$_POST["action"];
                    $data=$_POST["data"];
                    $return=load_bll("authentication_bll",$action,$data);
                restore_error_handler();
            echo json_encode($return);
        }
    }

