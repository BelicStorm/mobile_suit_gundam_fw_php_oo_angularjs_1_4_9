[Index](index.md)

(Estas funcionalidades cuentan con su respectivo [Test](../../README.md#TESTS))
# Routing & Autoload 
Estas dos funcionalidades son la base de toda la arquitectura backend de esta aplicación.
El autoload es el encargado de cargar todas las clases de la aplicación y el router el encargado de verificar si el módulo de la función a la que se ha llamado existe y en sí mismo de verificar, en el caso de que el modulo exista, de que esta misma exista en la aplicación.
## **Autoload**
---
Esta funcionalidad se divide en dos ficheros: autoload.php y paths.php.

### **Paths**
---
El fichero paths nos brinda una serie de constantes, las cuales albergan las direcciones que son propensas a contener clases.

***Funcionamiento: ***

    1. $path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_AngularJS_1_4_9/pagina/backend/';

    2. define('SITE_ROOT', $path);
    
    //Classes Path
    3. define('Classes_ROOT', SITE_ROOT."classes/");
        //BD_Classes
           3.1 define('bd_Root',Classes_ROOT."bd/");
           3.2 define('bd_bll',bd_Root."BLLs/");
           3.3 define('bd_conf',bd_Root."conf_and_connection/");
           3.4 define('bd_utils',bd_Root."utils/");
           3.5 define('bd_DAO',bd_Root."DAO/");
        //BD_Classes

### **Autoload**
---
El fichero autoload nos brinda las dos funcionalidades, la primera de ellas es la carga de todas las librerías de funciones de la aplicación; la segunda de ellas es una función mediante la cual usaremos las constantes del fichero path para cargar las clases de la aplicación. Por último el fichero autoload arrancaría el router.

***Funcionamiento:***

    function require_path($Path,$className,$extension){
        if (file_exists($Path . $className . $extension)) {
            require($Path. $className . $extension);
            set_include_path($Path);
            spl_autoload($className);
        }
    } 
    spl_autoload_register(function($class_name){
        require_path(Classes_ROOT,$class_name,'.class.singletone.php');
        
        /* BD Clases */
            require_path(bd_Root,$class_name,'.php');
            require_path(bd_bll,$class_name,'.class.singletone.php');
            require_path(bd_conf,$class_name,'.class.singletone.php');
            require_path(bd_utils,$class_name,'.php');
            require_path(bd_DAO,$class_name,'.class.singletone.php');
        /* BD Classes */
    }
     Router::run();

## **Router**
---
En paralelo tenemos funcionando el Router, el cual es el encargado de verificar que los módulos, de las funciones llamadas, existan y, en caso de ser así, llamarlas a funcionamiento. Para ello, el fichero .htacces nos brindara una sintaxis de URLs las cuales serán entendibles por el router.

***Funcionamiento:***

Al hacerse una llamada desde el frontend, el fichero .htacces redirigirá la llamada al index.php, a su vez el autoload cargara la clase y arrancara el router y aquí es donde este recogerá el modulo y la función y verificara que ambas existen.

|Código| Explicación|
|---|---|
| ![html](../media/router0.PNG)  | run()  se encarga de llamar a verificar el modulo y la función pasados por url y por ultimo llamar a la función resultante.   |
| ![html](../media/router1.PNG) ![html](../media/router2.PNG)  |  Estos dos métodos se encargan de verificar que los módulos o funciones estén registrados en los ficheros de allowed_functions o allowed_modules  |
| ![html](../media/router3.PNG)  |  Por último, run_function, es el encargado de llamar a la función resultante.  |