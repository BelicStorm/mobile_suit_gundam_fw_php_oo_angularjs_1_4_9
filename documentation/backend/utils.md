[Index](index.md)

(Estas funcionalidades cuentan con su respectivo [Test](../../README.md#TESTS))

# Utils

Esta aplicacion tiene varias utilidades remarcables en el backend de las cuales la creacion automatica de tokens con JWT y el sistema de mensajeria son las mas remarcables.

>## **JWT**

Este funcionalidad es el encargado de generar los token que los usuarios usaran dentro de la aplicacion para mantener su identidad segura. Este consta de dos partes, una clase encargada de codificar o decodificar el token y una funcion, encargada de enviarle a la clase las caracteristicas del token que se va a generar.

|Clase                      |Funcion                    |
|---                        |---                        |
|![html](../media/jwt.PNG)  |![html](../media/jwt2.PNG) |

>## **Mail**

Este funcionalidad es la encargada de enviar los correos y sus respectivas estructuras. Esta compuesta por un grupo de funciones encargadas de identificar el emisor del mensage, el receptor de este, el asunto y el cuerpo que este generara al ser enviado.

***Funcionalidad:***

* Esta funcion es la encargada de controlar el formateo del mensaje y posteriormente enviarlo:

    * $structure sera la encargada de generar el cuerpo y funcionalidad del mensaje dependiendo del tipo de mensaje que se quiera enviar.

        function enviar_email($arr) {    
            $structure = call_user_func($arr['type'].'_mail',$arr);

    * Aqui diferenciaremos a que tipo de usuario se le essta enviando el mensaje:

                try{
                    if ($arr['type'] === 'admin') 
                        $address = 'SOME_MAIL';
                    else
                        $address = $arr['inputEmail']; 

    * Por ultimo se enviara el mensaje:

                    $result = send_mailgun('mail', $address, $structure["subject"], $structure["html"]);    
                } catch (Exception $e) {
                    $return = 0;
                }
                return $result;
            }

* Cuando el call_user_function llame a alguna de las funciones constructoras del cuerpo del mensaje, estas recorreran el array, enviado como parametro y formatearan el cuerpo:

    * $subject,$ruta y $body, seran las variables encargadas de almacenar el cuerpo de nuestro mensaje:

            function contact_mail($message){
                $subject = 'Tu Petici&oacute;n  ha sido enviada<br>';
                $ruta = '<a href=' . 'SOME_URL'. '>aqu&iacute;</a>';
                $body = 'Para visitar nuestra web, pulsa ' . $ruta;
    
    * Tras esto generaremos un array con los datos amnteriores:

                $structure = array(
                    "subject" => $subject,
    
    * Y finalmente para crear el cuerpo del mail  llamaremos a la siguiente funcion:

                    "html" => build_mail_structure($subject,$body,$message['inputMessage'])
                );
                
                return $structure;
            }

* Por ultimo, y gracias a la tecnologia brindada por [MailGun](https://www.mailgun.com/), enviaremos el mensaje.

