[Index](index.md)

(Estas funcionalidades cuentan con su respectivo [Test](../../README.md#TESTS))

# ORM

El módulo de ORM es el encargado de reunir todas las clases responsables de la lógica de negocio de las diferentes funcionalidades con el acceso a la base de datos.

Su funcionamiento reside en:
1. Los controladores llaman a un fichero, encargado de cargar los BLL (common.inc.php).
2. Este llama a al bll, el cual se encargara de formatear los datos.
3. El fichero responsable de ejecutar las sentencias de base de datos (DAO.class.singletone.php) usara estos datos formateados para crearlas.

>## **1. Load BLL**
---
Esta parte del ORM es la encargada de unir la capa de BLLs con el resto de la aplicación.

***Funcionamiento:***

1. En el controlador pondremos estas líneas (No en todos los casos usamos esta sintaxis):

    * $action sera el encargado de almacenar que método del BLL va a llamarse

        $action=$_POST["action"];

    * $data sera la variable encargada de almacenar los datos que el bll usara:

        $data=$_POST["data"];

    * load_bll es el nombre de la función encargada de conectar el controlador con los BLL

        $return=load_bll("authentication_bll",$action,$data);

2. Ya en la función: 

    * En primer lugar generamos la supuesta dirección que tiene que tener el bll en la aplicación usando las constantes almacenadas en path:

            function load_bll($model_name, $function, $arrArgument = ''){
                $model = bd_bll . $model_name . '.class.singletone.php';

    * Tras esto, comprueba que el fichero exista. Si el fichero existe comprueba que el método y de la clase pasada por parámetro existan:

            try {
                if (file_exists($model)) {
                    /* include_once($model); */
                    $modelClass = $model_name;

                    if (!method_exists($modelClass, $function)){
                        throw new myException("The Method sended doesn't exist");
                    }

    * En el caso de que todo haya ido bien y el método exista, instanciaremos la clase y llamaremos al método, pasándole los parámetros:

                    $obj = $modelClass::getInstance();
                    if (isset($arrArgument)){
                        return $obj->$function($arrArgument);
                    }
                } else {
                    throw new myException("The file doesn't exist");
                }
            } catch (myException $e) {
                echo $e->get_Message();
            }
        }

>## **2. Conexión y configuración de la base de datos**
---
El ORM tiene conexión directa con una base de datos relacional creada en MYSQL. Para que esto sea posible necesitamos dos clases: Conf y DB.

>>### **1. Conf Class**
---
La clase de configuración se encarga de recoger las credenciales necesarias para conectar el backend con la base de datos. Estas credenciales se encuentran en un fichero llamado connect.ini.

![html](../media/conf_class.PNG)

>>### **2. DB Class**
---
La clase DB se encarga de establecer la conexión con la base de datos con las credenciales obtenidas en la clase Conf y de ejecutar las sentencias Sql.
Las sentencias de ejecución están divididas en dos: la encargada de, solamente, ejecutar las sentencias  (inserts, deletes etc), y la encargada de ejecutar y obtener respuestas de la base de datos (selects, shows etc).

![html](../media/db_class.PNG)

>## **3. Clases BLL**
---
Todas las clases BLL de la aplicación extienden de una clase llamada **core_bll**, la cual se encarga de almacenar las estancias de las clases que los BLL necesitan para operar de forma correcta:

![html](../media/core_bll.PNG)

Los bll almacenan la lógica de negocio de su módulo, pero para ejecutar el código necesario para seguir con el funcionamiento se crean dos clases: **common_sentences** y **DAO**.

>>### **1. DAO**
---
Esta clase es la encargada de generar las llamadas a los métodos que responsables de invocar las sentencias SQL.

***Funcionamiento:***

En este ejemplo de funcionamiento mostraremos los métodos encargados de generar la siguiente sentencia: 
*Select $rows form $table where $argument;*

Estos métodos se encargaran de almacenar en la variable content un string que equivaldrá al del ejemplo.

            public function select($rows,$tabla){
                $this->content= "SELECT $rows from $tabla";
            }
            public function where_argument($argument){
                $this->content= $this->content . " where " . $argument;
            }

Tras esto se llamara al siguiente método, el cual únicamente enviara el string generado a la clase de DB para ejecutarlo:

            public static function get($db){
                return $db->listar($db->ejecutar(self::$_instance->content));
            }

>>### **2. Common Sentences**
---
Esta clase es la encargada de reducir el número de llamadas repetidas a los métodos de la clase DAO.

***Funcionamiento:***

En este ejemplo de funcionamiento mostraremos los métodos encargados de generar la siguiente sentencia: 
*Select $rows form $table where $argument;*

        public function select_with_where($tabla,$rows,$where) {
            $this->dao->select("$rows","$tabla");
            $this->dao->where_argument("$where");
            return $this->dao->get($this->db, $this->dao->content);
        }

>>### **4. Ficheros BLL**
---

Todas las clases anteriores hacen posible que los BLL puedan operar correctamente:

***Funcionamiento:***

En este supuesto queremos que nos actualice el avatar de nuestra cuenta:

1. En este caso upload_image sera la variable $action que encontrábamos en el **load_bll** y $image, $data.

        public function upload_image($image){//actualizar avatar
            if (!$image["to_upload"]["upload"]) {
                return "no";
            }else{
                $url=Upload_files_ROOT."user_avatar";//url de la carpeta de los avatares
                $path=upload_image::upload_picture($url,$image["to_upload"]["upload"][0]["data"],$image["to_upload"]["upload"][0]["name"]);

2. Aquí ya empezamos a usar sentencias comunes:

    * La primera nos obtiene el id del usuario mediante su token:

                    $id=$this->common->get_user_token($image["token"],"")[0];
    
    * Con el id y el path de la imagen, actualizamos el avatar y el token del usuario

                    $this->common->update_avatar($id["user_id"],$path);
    
    * Obtenemos el nuevo token, generamos un array de respuesta y lo devolvemos para que el frontend se encargue del resto de la lógica:

                    $new_token=$this->common->get_user_token_by_id($id["user_id"])[0];
                    $data=array("return"=>"update_avatar","data"=>array("avatar"=>$path,"token"=>$new_token["user_token"])); 
                    return $data;
                }
            }
