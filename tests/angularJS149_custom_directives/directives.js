mainApp.directive("test",function(){
    var controller = function () {
        var vm = this;
    
        function init() {
            vm.items = [{}];
        }
        
        init();
        
        vm.addItem = function () {
            //Add new customer to directive scope
            vm.items=[{talk_to:"The parrot copies the speaker 2",name: vm.name},
                       {talk_to:"The parrot copies the speaker 1",name: vm.person1}];
            /* console.log(vm.person2);
            console.log(vm.items); */
        };
      };
      var template = '</br>Speaker 2 says:<input ng-model="vm.name" type="text">'+
                     '<button ng-click="vm.addItem()">Talk with the parrot</button>' +
                     '<ul><li ng-repeat="item in vm.items">{{::item.talk_to}}: {{ ::item.name }}</li></ul>';    

      return {
          restrict: 'EA', //Default for 1.3+
          scope: {
              person1: '@',
              /* add: '&', */
          },
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
          template: template
      };

});
/* 
En la declaración de los binging scope, cada tipo de bindeo se define por medio de un símbolo, los principales son:

"=": Sirve para entregar una referencia a un objeto. Por tanto cualquier cosa que se esté entregando se comparte entre componentes.

"@": Sirve para entregar un valor. No existe binding de ningún tipo.

"<": Sirve para bindear en una única dirección, o 1 way binding. El padre le transfiere al hijo un valor, pero aunque lo modifique 
     el hijo el nuevo valor no viaja al padre. Sin embargo, si el padre lo cambia, sí se cambia en el hijo.Si transfieres un objeto
     o un array, al pasarle la referencia en la práctica todavía estarás produciendo un 2 way binding tradicional de Angular.

"&": Permite enviar un puntero a una función.
 */