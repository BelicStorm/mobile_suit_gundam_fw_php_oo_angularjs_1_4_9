<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;
$path = $_SERVER['DOCUMENT_ROOT'] . '/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_mvc_jquery/pruebas_orm/bd/';


define('MODEL_PATH', SITE_ROOT . 'conf_and_connection/');

require(MODEL_PATH . "db.class.singletone.php");
require(SITE_ROOT . "DAO/DAO.class.singletone.php");

class profile_bll { 
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = DAO::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function select_all_users() {
        $arrArgument=$this->dao->select("*","user_name");
        return $this->dao->get($this->db, $this->dao->content);
    }

}