app.directive("calculadora",["dialog","$compile","$rootScope",function(dialog,$compile,$rootScope){
    var controller = function () {
        var vm = this;
        function init(params) {
             if(params){
                 vm.resultado=params;
             }
        }

        if($rootScope.resultado){
            init($rootScope.resultado)
        }
        
        vm.suma = function () {
            $rootScope.resultado=(vm.numero1+vm.numero2);
            dialog.showAdvanced('<calculadora type="output"></calculadora>');
        };
      };
      var template_button = '<input type="number" ng-model="vm.numero1" >'+
                            '<input type="number" ng-model="vm.numero2" >'+
                            '<input type="button" ng-click="vm.suma()" value="Sumar" >';
      var template_details= '<span>El resultado es {{vm.resultado}}</span>';
      function template(type){
            var pre_template="";
            switch (type) {
                case "input":
                    pre_template =  template_button;
                    break;
                case "output":
                    pre_template=  template_details;
                    break; 
                }
            return pre_template;
        };
        var linker = function(scope, element, attrs) {
            console.log(scope.vm);
            element.html(template(scope.vm.type)).show;
            $compile(element.contents())(scope);
        }   
      return {
          restrict: 'E', //Default for 1.3+
          scope: {
               type: '@',
          },
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
          /* template: template */
          link: linker
      };


}])