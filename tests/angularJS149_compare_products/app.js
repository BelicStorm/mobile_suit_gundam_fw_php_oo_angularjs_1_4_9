var app=angular.module('test', ['ngMaterial', 'ngMessages']);

app.controller('Compare_controller',function($scope){
    $scope.items = ["A", "B", "C", "D"];
    $scope.selected1 = "";
    $scope.selected2 = "";
     $scope.select = function(item) {
        if (!$scope.selected1) {
            $scope.selected1 = item;
            return;
        }
        
        // second select
        if (!$scope.selected2) {
            if ($scope.selected1 == item) {
                $scope.selected1 = "";
                return;
            }else {
                $scope.selected2 = item;
                $scope.compare = {item1:$scope.selected1,item2:$scope.selected2};
                return;
            }
        }
         // back to first select again
        $scope.selected1 = item;
        $scope.selected2 = "";
        $scope.compare = {item1:"",item2:""};
     }
    
});
app.directive("test",function(){
    var controller = function () {
        var vm = this;
        var items= [];
        
        vm.addItem = function () {
            console.log(vm.id);
        };
      };
      var template = '<button ng-click="vm.addItem()">Compare</button>';    

      return {
          restrict: 'E', //Default for 1.3+
          scope: {
              id: '@',
              /* add: '&', */
          },
          controller: controller,
          controllerAs: 'vm',
          bindToController: true, //required in 1.3+ with controllerAs
          template: template
      };

});