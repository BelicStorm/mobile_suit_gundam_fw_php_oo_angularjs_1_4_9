<?php
class core_bll {
    public $dao;
    public $db;
    public $common;
    static $_instance;

    public function __construct() {
        $this->dao = DAO::getInstance();
        $this->db = db::getInstance();
        $this->common = common_sentence::getInstance();
    }
    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    
}
