<?php
class home_bll extends core_bll { 
    static $_instance;

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function simple_select(){
        try {
            if (!method_exists("DAO", "select")){
                throw new myException("The Method in DAO doesn't exist");
            }
            $dao_instance=$this->dao;
            /* $dao_instance->insert("test_select");
            $dao_instance->start_value_insert_into("Nuevo Texto1");
            $dao_instance->more_values_insert_into("Nuevo Texto2");
            $dao_instance->close_insert_into_statement();
            $dao_instance->put($this->db, $dao_instance->content); */

            /* $dao_instance->update("test_select");
            $dao_instance->update_set("texto1","Actualizado 1");
            $dao_instance->more_update_set("texto2","Actualizado 2");
            $dao_instance->put($this->db, $dao_instance->content); */

            
            $array=array(array("column"=>"texto1","argument"=>"Hola"));
            $this->common->simple_update("test_select","texto1='Nuevo Texto1'",$array);
 
            $dao_instance->select("*","test_select");
            return $dao_instance->get($this->db, $dao_instance->content);


        } catch (myException $e) {
            $e->get_Message();
            return $e->show_simple_error();
        }
    }
   

}