<?php
  function load_bll($model_name, $function, $arrArgument = ''){
    $model = BLL_ROOT . $model_name . '.class.singletone.php';

    try {
        if (file_exists($model)) {
            /* include_once($model); */
            $modelClass = $model_name;

            if (!method_exists($modelClass, $function)){
                throw new myException("The Method sended doesn't exist");
            }

            $obj = $modelClass::getInstance();
            if (isset($arrArgument)){
                return $obj->$function($arrArgument);
            }
        } else {
            throw new myException("The file doesn't exist");
        }
    } catch (myException $e) {
        $e->get_Message();
        return $e->show_simple_error();
    }
  }
