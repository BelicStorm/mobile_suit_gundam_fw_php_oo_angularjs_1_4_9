<?php
  class Router
  {
    public static $module = "";
    public static $function="";

    public static function run(){
       if(!self::verify_module()){
         self::$module="E_404";
       }
       if(!self::verify_function()){
         self::$function="doSomething";
       }

      self::run_function();
    }

    protected static function verify_module(){

      if (!empty($_GET['module'])) {
        self::$module = $_GET['module'];
      } else {
        self::$module = 'home';
      }

      $modules = simplexml_load_file('resources/allowed_modules.xml');
      $exist = false;
        foreach ($modules->module as $module) {
          if ((self::$module == (String) $module->name)) {
             return true;
          }
        }
      return $exist;
    }
    protected static function verify_function(){
      if (!empty($_GET['function'])) {
          self::$function = $_GET['function'];
      } else {
          self::$function = 'print_home';
      }

      $functions = simplexml_load_file('resources/allowed_functions.xml');
      $function=self::$function;
      $exist = false;
        foreach ($functions->function as $module_funct) {
          if ((self::$function == (String) $module_funct->name)) {
             return true;
          }
        }
        return $exist;
    }

    protected static function run_function(){
      $controllerClass = self::$module;
      $obj = new $controllerClass;
      call_user_func(array($obj, self::$function));
    }
    
  }
 ?>