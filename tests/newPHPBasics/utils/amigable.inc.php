<?php
    function amigable($url, $return = false) {
        $amigablesOn = URL_AMIGABLES;
        $link = "";
        if ($amigablesOn) {
            $url = explode("&", str_replace("?", "", $url));
            foreach ($url as $key => $value) {
                $aux = explode("=", $value);
                if (!$aux){
                    $link .=  $aux[1]."/";
                }else {
                    $link .= "/". $aux[1];
                }
            }
        } else {
            $link = "index.php" . $url;
        }
        if ($return) {
            return Amigable_PATH . $link;
        }
        echo Amigable_PATH . $link;
    }
?>