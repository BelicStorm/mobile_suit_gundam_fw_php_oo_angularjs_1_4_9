CREATE DATABASE  IF NOT EXISTS `mobile_suit_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mobile_suit_db`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: mobile_suit_db
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bandos`
--

DROP TABLE IF EXISTS `bandos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bandos` (
  `bando_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `nombre` enum('Principado de Zeon','Federacion Terrestre') CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Principado de Zeon',
  `descripcion` text,
  PRIMARY KEY (`bando_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bandos`
--

LOCK TABLES `bandos` WRITE;
/*!40000 ALTER TABLE `bandos` DISABLE KEYS */;
INSERT INTO `bandos` VALUES (1,'Principado de Zeon','El Principado de Zeon, también conocido como el Ducado de Zeon, es una nación que aparece en Mobile Suit Gundam. Toma el control de las colonias de Side 3 y lucha contra la Federación Terrestre durante la Guerra de un Año.'),(2,'Federacion Terrestre','La Federación de la Tierra es un gobierno global presentado en la serie de televisión Mobile Suit Gundam. La Federación es una de las principales facciones de la línea de tiempo Universal Century.');
/*!40000 ALTER TABLE `bandos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobile_suit`
--

DROP TABLE IF EXISTS `mobile_suit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_suit` (
  `ms_modelo_id` int(11) NOT NULL,
  `ms_name` varchar(100) NOT NULL,
  `tamaño` varchar(20) DEFAULT NULL,
  `tamaño_Total` varchar(20) DEFAULT NULL,
  `peso_Total` varchar(20) DEFAULT NULL,
  `peso_Vacio` varchar(20) DEFAULT NULL,
  `velocidad` varchar(20) DEFAULT NULL,
  `energia` varchar(20) DEFAULT NULL,
  `encendido` varchar(20) DEFAULT NULL,
  `rango_Sensor` varchar(20) DEFAULT NULL,
  `pilots` enum('1','2') DEFAULT '1',
  `price_per_unit` varchar(20) NOT NULL,
  `production_date` varchar(10) NOT NULL,
  PRIMARY KEY (`ms_modelo_id`,`ms_name`),
  CONSTRAINT `fk_ms_modelo` FOREIGN KEY (`ms_modelo_id`) REFERENCES `modelo` (`modelo_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobile_suit`
--

LOCK TABLES `mobile_suit` WRITE;
/*!40000 ALTER TABLE `mobile_suit` DISABLE KEYS */;
INSERT INTO `mobile_suit` VALUES (1,'129_Geara_Zulu','18','20','55','22','35','2160','600','16400','1','1500','28/02/2019'),(1,'GearaDoga','18','20','50','23','36','2470','650','18200','1','1900','28/02/2019'),(2,'003_Gaza-C','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(2,'004G_Qubeley','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(2,'006_Gaza-D','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(2,'009_DomIII','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(2,'011_Zaku_III','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(2,'014_Doven_Wolf','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(3,'03_Bugu','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(4,'G-Cannon_Magna','1','1','1','1','1','1','1','1','1','200','28/02/2019'),(4,'G-Cannon_Powered_Weapon_Type','1','1','1','1','1','1','1','1','1','200','28/02/2019'),(4,'G_Cannon','1','1','1','1','1','1','1','1','1','200','28/02/2019'),(5,'02_Gun-EZ','1','1','1','1','1','1','1','1','1','200','28/02/2019'),(5,'02_Gun-EZ_Early_Production_Type','1','1','1','1','1','1','1','1','1','200','28/02/2019'),(5,'02_Gun-EZ_Land_Use_Type','1','1','1','1','1','1','1','1','1','200','28/02/2019'),(5,'G-Cannon_Magna','1','1','1','1','1','1','1','1','1','200','28/02/2019'),(6,'05_Zaku','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(6,'06-F2_ZakuII','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(6,'06-JC_Zaku_II','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(6,'06D_Desert_Zaku','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(6,'06_Zaku2','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(6,'07B_Gouf','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(6,'11_Act_Zaku','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(6,'14A_Gelgoog','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(6,'21C_Dra-C','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(6,'F_Gelgoog_Marine','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(6,'J-Sleeves','1','1','1','1','1','1','1','1','1','90','28/02/2019'),(7,'03C_HY-GOGG','1','1','1','1','1','1','1','1','1','900','28/02/2019'),(7,'07_Z\'Gok','1','1','1','1','1','1','1','1','1','190','28/02/2019'),(8,'003_Nemo','1','1','1','1','1','1','1','1','1','900','28/02/2019'),(8,'009_Gemeaux','1','1','1','1','1','1','1','1','1','900','28/02/2019'),(9,'006C1_Ζeta_Plus_C1','1','1','1','1','1','1','1','1','1','900','28/02/2019'),(9,'013_Mass_Production_Type_ZZ_Gundam','1','1','1','1','1','1','1','1','1','900','28/02/2019'),(9,'MSZ-79','1','1','1','1','1','1','1','1','1','900','28/02/2019');
/*!40000 ALTER TABLE `mobile_suit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelo`
--

DROP TABLE IF EXISTS `modelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelo` (
  `modelo_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(10) NOT NULL,
  `bando` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`modelo_id`),
  UNIQUE KEY `AK_nombre_modelo` (`nombre`),
  KEY `fk_bando_modelo` (`bando`),
  CONSTRAINT `fk_bando_modelo` FOREIGN KEY (`bando`) REFERENCES `bandos` (`bando_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelo`
--

LOCK TABLES `modelo` WRITE;
/*!40000 ALTER TABLE `modelo` DISABLE KEYS */;
INSERT INTO `modelo` VALUES (1,'AMS',1),(2,'AMX',1),(3,'CCMS',1),(4,'F71',2),(5,'LM111E',2),(6,'MS',1),(7,'MSM',1),(8,'MSS',2),(9,'MSZ',2);
/*!40000 ALTER TABLE `modelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock_shop_table`
--

DROP TABLE IF EXISTS `stock_shop_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_shop_table` (
  `user_id` varchar(100) NOT NULL,
  `ms_modelo_id` int(11) NOT NULL,
  `ms_name` varchar(100) NOT NULL,
  `stock` int(11) NOT NULL,
  `custom_price` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`ms_modelo_id`,`ms_name`),
  KEY `fk_shop_product` (`ms_modelo_id`,`ms_name`),
  CONSTRAINT `fk_shop_product` FOREIGN KEY (`ms_modelo_id`, `ms_name`) REFERENCES `mobile_suit` (`ms_modelo_id`, `ms_name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_shop_reference` FOREIGN KEY (`user_id`) REFERENCES `user_id` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock_shop_table`
--

LOCK TABLES `stock_shop_table` WRITE;
/*!40000 ALTER TABLE `stock_shop_table` DISABLE KEYS */;
INSERT INTO `stock_shop_table` VALUES ('tienda@gmail.com_ZeonicIndustries',1,'GearaDoga',1,1900),('tienda@gmail.com_ZeonicIndustries',2,'003_Gaza-C',1,90),('tienda@gmail.com_ZeonicIndustries',2,'011_Zaku_III',1,90),('tienda@gmail.com_ZeonicIndustries',2,'014_Doven_Wolf',1,90),('tienda@gmail.com_ZeonicIndustries',3,'03_Bugu',1,90),('tienda@tienda.com_LondoBell',2,'004G_Qubeley',100,80),('tienda@tienda.com_LondoBell',7,'03C_HY-GOGG',1,900),('tienda@tienda.com_LondoBell',8,'009_Gemeaux',1,900),('tienda@tienda.com_LondoBell',9,'006C1_Ζeta_Plus_C1',1,900),('tienda@tienda.com_LondoBell',9,'MSZ-79',4,900);
/*!40000 ALTER TABLE `stock_shop_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_avatar`
--

DROP TABLE IF EXISTS `user_avatar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_avatar` (
  `user_id` varchar(100) NOT NULL,
  `user_avatar` longtext,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_avatar` FOREIGN KEY (`user_id`) REFERENCES `user_id` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_avatar`
--

LOCK TABLES `user_avatar` WRITE;
/*!40000 ALTER TABLE `user_avatar` DISABLE KEYS */;
INSERT INTO `user_avatar` VALUES ('admin@gmail.com_Admin1','http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_AngularJS_1_4_9/pagina/frontend/media/info/Fotos/profile_pics/pic_icon1.png'),('cpardoca3@gmail.com_Cliente1','http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_AngularJS_1_4_9/pagina/backend/media/user_avatar/1368326681109.png'),('tienda@gmail.com_ZeonicIndustries','http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_AngularJS_1_4_9/pagina/frontend/media/info/Fotos/profile_pics/pic_icon7.png'),('tienda@tienda.com_LondoBell','http://localhost/htdoc_daw1/ejercicios/11/pagina/mobile_suit_gundam_fw_php_oo_AngularJS_1_4_9/pagina/backend/media/user_avatar/s-l300.jpg');
/*!40000 ALTER TABLE `user_avatar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_control`
--

DROP TABLE IF EXISTS `user_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_control` (
  `user_id` varchar(100) NOT NULL,
  `user_activated` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_control` FOREIGN KEY (`user_id`) REFERENCES `user_id` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_control`
--

LOCK TABLES `user_control` WRITE;
/*!40000 ALTER TABLE `user_control` DISABLE KEYS */;
INSERT INTO `user_control` VALUES ('admin@gmail.com_Admin1',1),('cpardoca3@gmail.com_Cliente1',1),('tienda@gmail.com_ZeonicIndustries',1),('tienda@tienda.com_LondoBell',1);
/*!40000 ALTER TABLE `user_control` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_id`
--

DROP TABLE IF EXISTS `user_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_id` (
  `user_id` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_id`
--

LOCK TABLES `user_id` WRITE;
/*!40000 ALTER TABLE `user_id` DISABLE KEYS */;
INSERT INTO `user_id` VALUES ('admin@gmail.com_Admin1'),('cpardoca3@gmail.com_Cliente1'),('tienda@gmail.com_ZeonicIndustries'),('tienda@tienda.com_LondoBell');
/*!40000 ALTER TABLE `user_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_mail`
--

DROP TABLE IF EXISTS `user_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_mail` (
  `user_id` varchar(100) NOT NULL,
  `user_mail` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`,`user_mail`),
  CONSTRAINT `fk_user_mail` FOREIGN KEY (`user_id`) REFERENCES `user_id` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_mail`
--

LOCK TABLES `user_mail` WRITE;
/*!40000 ALTER TABLE `user_mail` DISABLE KEYS */;
INSERT INTO `user_mail` VALUES ('admin@gmail.com_Admin1','admin@gmail.com'),('cpardoca3@gmail.com_Cliente1','cpardoca3@gmail.com'),('tienda@gmail.com_ZeonicIndustries','tienda@gmail.com'),('tienda@tienda.com_LondoBell','tienda@tienda.com');
/*!40000 ALTER TABLE `user_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_name`
--

DROP TABLE IF EXISTS `user_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_name` (
  `user_id` varchar(100) NOT NULL,
  `user_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_name` FOREIGN KEY (`user_id`) REFERENCES `user_id` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_name`
--

LOCK TABLES `user_name` WRITE;
/*!40000 ALTER TABLE `user_name` DISABLE KEYS */;
INSERT INTO `user_name` VALUES ('admin@gmail.com_Admin1','Admin1'),('cpardoca3@gmail.com_Cliente1','Cliente1'),('tienda@gmail.com_ZeonicIndustries','ZeonicIndustries'),('tienda@tienda.com_LondoBell','LondoBell');
/*!40000 ALTER TABLE `user_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_password`
--

DROP TABLE IF EXISTS `user_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_password` (
  `user_id` varchar(100) NOT NULL,
  `user_password` longtext,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_password` FOREIGN KEY (`user_id`) REFERENCES `user_id` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_password`
--

LOCK TABLES `user_password` WRITE;
/*!40000 ALTER TABLE `user_password` DISABLE KEYS */;
INSERT INTO `user_password` VALUES ('admin@gmail.com_Admin1','Aaaaaaa@1'),('cpardoca3@gmail.com_Cliente1','Aaaaaa1@'),('tienda@gmail.com_ZeonicIndustries','Aaaaaaa@1'),('tienda@tienda.com_LondoBell','TiendaTest');
/*!40000 ALTER TABLE `user_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_token`
--

DROP TABLE IF EXISTS `user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_token` (
  `user_id` varchar(100) NOT NULL,
  `user_token` longtext CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user_id` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_token`
--

LOCK TABLES `user_token` WRITE;
/*!40000 ALTER TABLE `user_token` DISABLE KEYS */;
INSERT INTO `user_token` VALUES ('admin@gmail.com_Admin1','eyJ0eXAiOiJKV1QiLCAiYWxnIjoiSFMyNTYifQ.ew0KICAgICAgICAnaWF0Jzp0aW1lKCksIA0KICAgICAgICAnZXhwJzp0aW1lKCkgKyAoNjAqNjApLA0KICAgICAgICAnbmFtZSc6IGFkbWluQGdtYWlsLmNvbV9BZG1pbjENCiAgICAgICAgfQ.QH9gWOeM9vA_TfZ1OTBHyymqigNI-WGD7t-ikfMJGAI'),('cpardoca3@gmail.com_Cliente1','eyJ0eXAiOiJKV1QiLCAiYWxnIjoiSFMyNTYifQ.ew0KICAgICAgICAnaWF0Jzp0aW1lKCksIA0KICAgICAgICAnZXhwJzp0aW1lKCkgKyAoNjAqNjApLA0KICAgICAgICAnbmFtZSc6IGh0dHA6Ly9sb2NhbGhvc3QvaHRkb2NfZGF3MS9lamVyY2ljaW9zLzExL3BhZ2luYS9tb2JpbGVfc3VpdF9ndW5kYW1fZndfcGhwX29vX0FuZ3VsYXJKU18xXzRfOS9wYWdpbmEvYmFja2VuZC9tZWRpYS91c2VyX2F2YXRhci8xMzY4MzI2NjgxMTA5LnBuZw0KICAgICAgICB9.pmyJe8h-Iv3ihDJp7Bw8h6QkkZNEF6lZ-1fN8KPT6T0'),('tienda@gmail.com_ZeonicIndustries','eyJ0eXAiOiJKV1QiLCAiYWxnIjoiSFMyNTYifQ.ew0KICAgICAgICAnaWF0Jzp0aW1lKCksIA0KICAgICAgICAnZXhwJzp0aW1lKCkgKyAoNjAqNjApLA0KICAgICAgICAnbmFtZSc6IHRpZW5kYUBnbWFpbC5jb21fWmVvbmljSW5kdXN0cmllcw0KICAgICAgICB9.fkE50NKPEC48_UUSqJmrTbWZI4VNxR_D0XP5BfIfP_E'),('tienda@tienda.com_LondoBell','eyJ0eXAiOiJKV1QiLCAiYWxnIjoiSFMyNTYifQ.ew0KICAgICAgICAnaWF0Jzp0aW1lKCksIA0KICAgICAgICAnZXhwJzp0aW1lKCkgKyAoNjAqNjApLA0KICAgICAgICAnbmFtZSc6IGh0dHA6Ly9sb2NhbGhvc3QvaHRkb2NfZGF3MS9lamVyY2ljaW9zLzExL3BhZ2luYS9tb2JpbGVfc3VpdF9ndW5kYW1fZndfcGhwX29vX0FuZ3VsYXJKU18xXzRfOS9wYWdpbmEvYmFja2VuZC9tZWRpYS91c2VyX2F2YXRhci9zLWwzMDAuanBnDQogICAgICAgIH0.7MMLVeJN-LSBMCGlHhHrJXc5ZeuSFfc5yMAUeGZ9_Ao');
/*!40000 ALTER TABLE `user_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'mobile_suit_db'
--

--
-- Dumping routines for database 'mobile_suit_db'
--
/*!50003 DROP PROCEDURE IF EXISTS `create_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_user`( 
	in id varchar(100), 
    in Uname varchar(30), 
    in Uavatar longtext, 
    in Umail varchar(100), 
    in Upassword longtext, 
    in Utoken longtext, 
    in Ucontrol boolean)
BEGIN
	START TRANSACTION;
			INSERT INTO user_id VALUES (id);
			INSERT INTO user_name VALUES (id, Uname);
			INSERT INTO user_avatar VALUES (id, Uavatar);
			INSERT INTO user_mail VALUES (id, Umail);
			INSERT INTO user_password VALUES (id, Upassword);
			INSERT INTO user_token VALUES (id, Utoken);
			INSERT INTO user_control VALUES (id, Ucontrol);
		COMMIT;
 END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_stock` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_stock`(in shop_name varchar (90),in porduct_name varchar (100))
BEGIN
		IF !isnull(shop_name) and !isnull(porduct_name)   THEN
			BEGIN
			  Select 	  user_name.user_name,
			  modelo.modelo_id,
			  modelo.nombre as modelo,
			  mobile_suit.ms_name as ms_name,
			  mobile_suit.tamaño,
			  mobile_suit.tamaño_Total,
			  mobile_suit.peso_Total,
			  mobile_suit.peso_Vacio,
			  mobile_suit.velocidad,
			  mobile_suit.energia,
			  mobile_suit.encendido,
			  mobile_suit.rango_Sensor,
			  mobile_suit.pilots,
			  mobile_suit.production_date,
			  stock,custom_price
			  from stock_shop_table 
			  inner join user_name on  stock_shop_table.user_id = user_name.user_id 
			  inner join modelo on  stock_shop_table.ms_modelo_id = modelo.modelo_id
			  inner join mobile_suit on  stock_shop_table.ms_name = mobile_suit.ms_name
              where user_name.user_name = shop_name and mobile_suit.ms_name = porduct_name;
			END;
		ELSE
			BEGIN
				Select 	  user_name.user_name,
				  modelo.modelo_id,
				  modelo.nombre as modelo,
				  mobile_suit.ms_name as ms_name,
				  mobile_suit.tamaño,
				  mobile_suit.tamaño_Total,
				  mobile_suit.peso_Total,
				  mobile_suit.peso_Vacio,
				  mobile_suit.velocidad,
				  mobile_suit.energia,
				  mobile_suit.encendido,
				  mobile_suit.rango_Sensor,
				  mobile_suit.pilots,
				  mobile_suit.production_date,
				  stock,custom_price
				  from stock_shop_table 
				  inner join user_name on  stock_shop_table.user_id = user_name.user_id 
				  inner join modelo on  stock_shop_table.ms_modelo_id = modelo.modelo_id
				  inner join mobile_suit on  stock_shop_table.ms_name = mobile_suit.ms_name;  
			END;
		END if;
 END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user`( in gived_id varchar(100))
BEGIN
	START TRANSACTION;
		select user_id.user_id,
			  user_name.user_name,
			  user_avatar.user_avatar,
			  user_mail.user_mail,
			  user_password.user_password,
			  user_token.user_token,
			  user_control.user_activated
		from user_id inner join user_name on user_id.user_id = user_name.user_id
			  inner join user_avatar on user_id.user_id = user_avatar.user_id
			  inner join user_mail on user_id.user_id = user_mail.user_id
			  inner join user_password on user_id.user_id = user_password.user_id
			  inner join user_token on user_id.user_id = user_token.user_id
			  inner join user_control on user_id.user_id = user_control.user_id
              where user_id.user_id=gived_id;
	COMMIT;
 END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-04 21:08:10
